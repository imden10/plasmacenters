<?php

if (!function_exists('plural_form')) {

    function plural_form($number, $after) {
        $cases = [2, 0, 1, 1, 1, 2];
        return $number . ' ' . $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }

}

if (!function_exists('convert_array_field_name_to_with_dot')) {

    function convert_array_field_name_to_with_dot(string $name) {
        $modified = preg_replace('/\]\[|\[|\]/', '.', $name);

        return $modified ? rtrim($modified, '.') : $name;
    }

}

if (!function_exists('media_preview_box')) {

    function media_preview_box($name, $path = null, $errors = null) { ?>
        <div class="media-wrapper" style="width: 200px">
            <div class="text-center">
                <img src="<?php echo get_image_uri(old(convert_array_field_name_to_with_dot($name), $path ?? '')); ?>" class="img-thumbnail image-tag <?php if ($errors && $errors->first(convert_array_field_name_to_with_dot($name))) : ?> border-danger <?php endif; ?>">
            </div>

            <input type="hidden" name="<?php echo $name; ?>" value="<?php echo old(convert_array_field_name_to_with_dot($name), $path ?? ''); ?>" class="media-input">

            <?php if ($errors && $errors->first(convert_array_field_name_to_with_dot($name))) : ?>
                <span class="invalid-feedback d-block" role="alert">
                    <strong><?php echo $errors->first(convert_array_field_name_to_with_dot($name)); ?></strong>
                </span>
            <?php endif; ?>

            <div class="mt-1 text-center">
                <button type="button" class="btn btn-outline-info btn-sm choice-media">Выбрать</button>

                <button type="button" class="btn btn-outline-danger btn-sm remove-media">Удалить</button>
            </div>
        </div>
    <?php }

}

if (!function_exists('file_preview_box')) {

    function file_preview_box($name, $path = null, $errors = null) { ?>
        <div class="media-wrapper" style="width: 136px">
            <div class="text-center">
                <img src="<?php echo get_image_uri(old(convert_array_field_name_to_with_dot($name), $path ?? '')); ?>" class="img-thumbnail image-tag <?php if ($errors && $errors->first(convert_array_field_name_to_with_dot($name))) : ?> border-danger <?php endif; ?>">
            </div>

            <input type="hidden" name="<?php echo $name; ?>" value="<?php echo old(convert_array_field_name_to_with_dot($name), $path ?? ''); ?>" class="file-path-field">

            <?php if ($errors && $errors->first(convert_array_field_name_to_with_dot($name))) : ?>
                <span class="invalid-feedback d-block" role="alert">
                    <strong><?php echo $errors->first(convert_array_field_name_to_with_dot($name)); ?></strong>
                </span>
            <?php endif; ?>

            <div class="mt-1 text-center">
                <button type="button" class="btn btn-outline-info btn-sm choice-file">Выбрать</button>

                <button type="button" class="btn btn-outline-danger btn-sm remove-file">Удалить</button>
            </div>
        </div>
    <?php }

}
