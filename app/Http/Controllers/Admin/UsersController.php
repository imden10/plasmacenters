<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MeterReading;
use App\Models\Receipts;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\ReceipsUploadRequest;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Database\Eloquent\Builder;
use Throwable;
use Exception;

class UsersController extends Controller
{
    /**
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $status = $request->get('status');

        $users = User::query()->leftJoin('roles', 'roles.id', '=', 'users.role_id')
            ->select([
                'users.*'
            ])
            ->orderBy('users.created_at', 'desc')
            ->where(function ($q) {
                /* @var $q Builder */
                $q->where('users.role_id', 2);
            })
            ->where(function ($q) use ($status) {
                /* @var $q Builder */
                if ($status != '') {
                    $q->where('users.status', $status);
                }
            })
            ->paginate(20);

        return view('admin.users.index', [
            'users' => $users
        ]);
    }

    /**
     * @return Factory|View
     */
    public function getAdminUsers()
    {

        $users = User::query()
            ->leftJoin('roles', 'roles.id', '=', 'users.role_id')
            ->select([
                'users.*',
                'roles.name AS role_name'
            ])
            ->orderBy('users.created_at', 'desc')
            ->where(function ($q) {
                /* @var $q Builder */
                $q->where('users.role_id', 1);
            })
            ->paginate(20);

        return view('admin.users-admin.index', [
            'users' => $users
        ]);
    }

    /**
     *
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param User $user
     * @return Factory|View
     */
    public function show(User $user)
    {
        return view('admin.users.tabs.index', [
            'model' => $user,
            'tab'   => 'main'
        ]);
    }

    /**
     * @param $id
     */
    public function edit($id)
    {
        //
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->back()->with('success', 'User deleted successfully');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function setStatus(Request $request)
    {
        $userId = $request->get('user_id');
        $status = $request->get('status');

        $user = User::query()->where('id', $userId)->first();

        $user->status = $status;

        if ($user->save()) {
            return redirect()->back()->with('success', 'Status changed!');
        } else {
            return redirect()->back()->with('error', 'Failed to change status!');
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function changeStatus(Request $request)
    {
        /* @var $model User */
        $model = User::query()
            ->where('id', $request->get('user_id'))
            ->first();

        if ($model) {

            $model->status = $request->get('status');

            if ($model->save()) {
                return redirect()->back()->with('success', 'Статус успішно змінено!');
            }
        }

        return redirect()->back()->with('error', 'Помилка зміни статусу!');
    }

    /**
     * @param $id
     * @param $tab
     * @return Factory|View
     */
    public function getTab($id, $tab)
    {
        $user = User::query()->where('id', $id)->first();

        if (!$user)
            abort(404);

        return view('admin.users.tabs.index', [
            'model' => $user,
            'tab'   => $tab
        ]);
    }
}
