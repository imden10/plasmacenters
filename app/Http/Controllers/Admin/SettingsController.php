<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BlogArticles;
use App\Models\Donors;
use App\Models\Langs;
use App\Models\Pages;
use App\Models\Plasmacenters;
use App\Models\Settings;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index($tab)
    {
        $model = Settings::query()
            ->select([
                'code', 'value', 'lang'
            ])
            ->get()
            ->groupBy('lang')->transform(function($item, $k) {
                return $item->groupBy('code');
            })
            ->toArray();

        $localizations = Langs::getLangsWithTitle();

        return view('admin.settings.index', [
            'data'          => $model,
            'tab'           => $tab,
            'localizations' => $localizations
        ]);
    }

    public function save(Request $request)
    {
        $post = $request->except(['_token']);

        foreach ($post['setting_data'] as $lang => $data){
            foreach ($data as $code => $value) {
                $item = Settings::firstOrNew([
                    'code' => $code,
                    'lang' => $lang
                ]);

                $newVal = $value;

                if (is_array($value)) {
                    $newVal = json_encode($value, JSON_UNESCAPED_UNICODE);
                }

                $item->value = $newVal;
                $item->save();

                if($lang == Langs::getDefaultLangCode()){
                    /* BLOG DEFAULT MENU*/
                    if(isset($data['blog_default_page_menu'])){
                        if(! is_null($data['blog_default_page_menu'])){
                            foreach (BlogArticles::query()->get() as $modelItem){
                                if(is_null($modelItem->menu_id)){
                                    $modelItem->menu_id = (int)$data['blog_default_page_menu'];
                                    $modelItem->save();
                                }
                            }
                        } else {
                            foreach (BlogArticles::query()->get() as $modelItem){
                                $modelItem->menu_id = null;
                                $modelItem->save();
                            }
                        }
                    }

                    /* PAGE DEFAULT MENU*/
                    if(isset($data['page_default_page_menu'])) {
                        if (!is_null($data['page_default_page_menu'])) {
                            foreach (Pages::query()->get() as $modelItem) {
                                if (is_null($modelItem->menu_id)) {
                                    $modelItem->menu_id = (int)$data['page_default_page_menu'];
                                    $modelItem->save();
                                }
                            }
                        } else {
                            foreach (Pages::query()->get() as $modelItem) {
                                $modelItem->menu_id = null;
                                $modelItem->save();
                            }
                        }
                    }

                    /* DONORS DEFAULT MENU*/
                    if(isset($data['donors_default_page_menu'])) {
                        if (!is_null($data['donors_default_page_menu'])) {
                            foreach (Donors::query()->get() as $modelItem) {
                                if (is_null($modelItem->menu_id)) {
                                    $modelItem->menu_id = (int)$data['donors_default_page_menu'];
                                    $modelItem->save();
                                }
                            }
                        } else {
                            foreach (Donors::query()->get() as $modelItem) {
                                $modelItem->menu_id = null;
                                $modelItem->save();
                            }
                        }
                    }

                    /* PLASMACENTERS DEFAULT MENU*/
                    if(isset($data['plasmacenters_default_page_menu'])) {
                        if (!is_null($data['plasmacenters_default_page_menu'])) {
                            foreach (Plasmacenters::query()->get() as $modelItem) {
                                if (is_null($modelItem->menu_id)) {
                                    $modelItem->menu_id = (int)$data['plasmacenters_default_page_menu'];
                                    $modelItem->save();
                                }
                            }
                        } else {
                            foreach (Plasmacenters::query()->get() as $modelItem) {
                                $modelItem->menu_id = null;
                                $modelItem->save();
                            }
                        }
                    }
                }
            }
        }

        return redirect()->back()->with('success', 'Настройки успешно сохранены!');
    }
}
