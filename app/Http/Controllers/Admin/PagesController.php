<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Langs;
use App\Models\Pages;
use App\Models\PagesMenu;
use App\Service\MenuHelper;
use Illuminate\Http\Request;
use App\Http\Requests\PagesRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class PagesController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $status = $request->get('status');
        $title = $request->get('title');

        $model = Pages::query()
            ->leftJoin('pages_translations', 'pages_translations.pages_id', '=', 'pages.id')
            ->where('pages_translations.lang',Langs::getDefaultLangCode())
            ->select([
                'pages.*',
                'pages_translations.title'
            ])
            ->orderBy('pages.status', 'desc')
            ->orderBy('pages.created_at', 'desc')
            ->where(function ($q) use ($status,$title) {
                if ($status != '') {
                    $q->where('pages.status', $status);
                }
                if ($title != '') {
                    $q->where('pages_translations.title', 'like', '%' . $title . '%');
                }
            })
            ->paginate(20);

        return view('admin.pages.index', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Pages();

        $model->is_share_block = 1;
        $model->is_sign_up_block = 1;

        $localizations = Langs::getLangsWithTitle();

        return view('admin.pages.create', [
            'model'         => $model,
            'localizations' => $localizations
        ]);
    }

    /**
     * @param PagesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $model = new Pages();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;
            $model->is_share_block = $request->get('is_share_block') ?? false;
            $model->is_sign_up_block = $request->get('is_sign_up_block') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []), $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('pages.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('pages.index')->with('success', 'Страница успешно добавлена!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Pages $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Pages $page)
    {
        $localizations = Langs::getLangsWithTitle();

//        $menu = PagesMenu::query()
//            ->where('page_id', $page->id)
//            ->orderBy('order', 'asc')
//            ->get();


        return view('admin.pages.edit', [
            'model'         => $page,
            'data'          => $page->getTranslationsArray(),
            'localizations' => $localizations
//            'menu'          => $menu
        ]);
    }

    /**
     * @param PagesRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        /* @var $model Pages */
        $model = Pages::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;
            $model->is_share_block = $request->get('is_share_block') ?? false;
            $model->is_sign_up_block = $request->get('is_sign_up_block') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            $model->deleteTranslations();

            $model->audio()->sync($request->get('audio'));

//            PagesMenu::query()->where('page_id', $model->id)->delete();

//            if ($request->has('menu')) {
//                $order = 0;
//                foreach ($request->get('menu') as $item) {
//                    $menu              = new PagesMenu();
//                    $menu->page_id     = $model->id;
//                    $menu->model_type  = $item['class'];
//                    $menu->model_id    = $item['id'];
//                    $menu->order       = $order;
//                    $menu->model_title = $item['title'];
//                    $menu->model_slug  = $item['slug'];
//
//                    if (!$menu->save()) {
//                        DB::rollBack();
//                    }
//
//                    $order++;
//                }
//            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []), $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('pages.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

//        $pageMenu = PagesMenu::query()
//            ->where('model_id',$model->id)
//            ->where('model_type',get_class($model))
//            ->get();
//
//        if(count($pageMenu)) {
//            foreach ($pageMenu as $pm){
//                $pm->model_title = $model->title;
//                $pm->model_slug = $model->slug;
//                $pm->save();
//            }
//        }


        return redirect()->back()->with('success', 'Страница успешно обновлена!');
    }

    /**
     * @param Pages $page
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Pages $page)
    {
        $page->deleteTranslations();
        $page->audio()->sync([]);
        $page->delete();

        return redirect()->back()->with('success', 'Страницу успешно удалено!');
    }

//    public function addItem(Request $request)
//    {
//        $val = $request->get('val');
//
//        $data = explode('_', $val);
//
//        $modelClass = MenuHelper::classes[$data[0]];
//
//        $modelId = $data[1];
//
//        $model = $modelClass::query()->where('id', $modelId)->first();
//
//        if (!$model) {
//            return '';
//        }
//
//        return View::make('admin.pages._menu_elem', [
//            'title'      => $model->title,
//            'modelId'    => $model->id,
//            'modelTitle' => $model->title,
//            'modelSlug'  => $model->slug,
//            'modelClass' => $modelClass,
//            'typeName'   => MenuHelper::names[$data[0]],
//            'randKey'    => rand(1000, 9999)
//        ]);
//    }
}
