<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Orders;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->get('status');
        $promo  = $request->get('promo');
        $search = $request->get('q');
        $date   = $request->get('p');

        /* Sorting */

        $sort  = 'created_at';
        $order = 'desc';

        if ($request->has('sort') && $request->has('order')) {
            $sort  = $request->get('sort');
            $order = $request->get('order');
        }

        $model = \App\Models\Request::query()
            ->orderBy($sort, $order)
            ->where(function ($q) use ($status, $promo, $search, $date) {
                if ($status != '') {
                    $q->where('status', $status);
                }

                if ($promo != '') {
                    $q->where('promo', $promo);
                }

                if ($search != '') {
                    $q->where('plasmacenter_name', 'like', '%' . $search . '%')
                        ->orWhere('phone', 'like', '%' . $search . '%');
                }
                if ($date) {
                    $dateFrom = substr($date, 0, 10);
                    $dateFrom = Carbon::parse($dateFrom)->format('Y-m-d') . ' 00:00:00';
                    $dateTo   = substr($date, 13, 10);
                    $dateTo   = Carbon::parse($dateTo)->format('Y-m-d') . ' 23:59:59';

                    $q->whereBetween('created_at', [$dateFrom, $dateTo]);
                }
            })
            ->paginate(20);

        $from = \App\Models\Request::query()->orderBy('created_at', 'asc')->get();
        $from = Carbon::parse($from[0]->created_at)->format('d-m-Y');

        $to = \App\Models\Request::query()->orderBy('created_at', 'desc')->get();
        $to = Carbon::parse($to[0]->created_at)->format('d-m-Y');

        return view('admin.request.index', [
            'model' => $model,
            'from'  => $from,
            'to'    => $to
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new \App\Models\Request();

        return view('admin.request.create', [
            'model' => $model
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datetime = Carbon::parse($request->get('datetime'))->format('Y-m-d H:i');

        $model = \App\Models\Request::create([
            'name'            => $request->get('name'),
            'phone'           => $request->get('phone'),
            'plasmacenter_id' => $request->get('plasmacenter_id'),
            'datetime'        => $datetime,
            'status'          => \App\Models\Request::STATUS_NEW
        ]);

        return redirect()->route('request.index')->with('success', 'Запись успешно создана!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = \App\Models\Request::query()->where('id', $id)->first();

        $model->datetime = Carbon::create($model->datetime)->format('d-m-Y H:i');

        return view('admin.request.edit', [
            'model' => $model
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* @var $model \App\Models\Request */
        $model = \App\Models\Request::query()->where('id', $id)->first();

        $datetime = Carbon::parse($request->get('datetime'))->format('Y-m-d H:i');

        $model->fill($request->all());

        $model->datetime = $datetime;

        $model->save();

        return redirect()->back()->with('success', 'Запись успешно обновлена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\App\Models\Request $request)
    {
        $request->delete();

        return redirect()->back()->with('success', 'Запись успешно удалено!');
    }

    public function changeStatus(Request $request)
    {
        /* @var $model \App\Models\Request */
        $model = \App\Models\Request::query()
            ->where('id', $request->get('request_id'))
            ->first();

        $model->status = $request->get('status');
        $model->save();

        return redirect()->back()->with('success', 'Статус успешно изменен!');
    }
}
