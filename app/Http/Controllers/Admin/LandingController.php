<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BlogArticles;
use App\Models\Landing;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Langs;

class LandingController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $status = $request->get('status');
        $title = $request->get('title');

        $model = Landing::query()
                ->leftJoin('landing_translations', 'landing_translations.landing_id', '=', 'landings.id')
                ->where('landing_translations.lang',Langs::getDefaultLangCode())
                ->select([
                    'landings.*',
                    'landing_translations.title'
                ])
            ->orderBy('landings.created_at', 'desc')
            ->where(function ($q) use ($status,$title) {
                if ($status != '') {
                    $q->where('landings.status', $status);
                }
                if ($title != '') {
                    $q->where('landing_translations.title', 'like', '%' . $title . '%');
                }
            })
            ->paginate(20);

        return view('admin.landing.index', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Landing();

        $localizations = Langs::getLangsWithTitle();

        return view('admin.landing.create', [
            'model'         => $model,
            'localizations' => $localizations
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $model = new Landing();

        DB::beginTransaction();

        try {
            if (empty($request->input('slug', ''))) {
                $request->merge(array('slug' => SlugService::createSlug(Landing::class, 'slug', $request->input('page_data.' . Langs::getDefaultLangCode() . '.title'))));
            }

            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []), $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('landing.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('landing.index')->with('success', 'Лендинг успешно добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Landing $landing
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Landing $landing)
    {
        $localizations = Langs::getLangsWithTitle();

        return view('admin.landing.edit', [
            'model'         => $landing,
            'data'          => $landing->getTranslationsArray(),
            'localizations' => $localizations
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        /* @var $model Landing */
        $model = Landing::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            $model->deleteTranslations();

            $model->audio()->sync($request->get('audio'));

//            PagesMenu::query()->where('page_id', $model->id)->delete();

//            if ($request->has('menu')) {
//                $order = 0;
//                foreach ($request->get('menu') as $item) {
//                    $menu              = new PagesMenu();
//                    $menu->page_id     = $model->id;
//                    $menu->model_type  = $item['class'];
//                    $menu->model_id    = $item['id'];
//                    $menu->order       = $order;
//                    $menu->model_title = $item['title'];
//                    $menu->model_slug  = $item['slug'];
//
//                    if (!$menu->save()) {
//                        DB::rollBack();
//                    }
//
//                    $order++;
//                }
//            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []), $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('landing.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

//        $pageMenu = PagesMenu::query()
//            ->where('model_id',$model->id)
//            ->where('model_type',get_class($model))
//            ->get();
//
//        if(count($pageMenu)) {
//            foreach ($pageMenu as $pm){
//                $pm->model_title = $model->title;
//                $pm->model_slug = $model->slug;
//                $pm->save();
//            }
//        }


        return redirect()->back()->with('success', 'Лендинг успешно обновлен!');
    }

    /**
     * @param Landing $landing
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Landing $landing)
    {
        $landing->deleteTranslations();
        $landing->audio()->sync([]);
        $landing->delete();

        return redirect()->back()->with('success', 'Лендинг успешно удалено!');
    }
}
