<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BlogArticles;
use App\Models\Donors;
use App\Models\Landing;
use App\Models\Plasmacenters;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Langs;
use Illuminate\Support\Facades\View;

class PlasmacenterController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $status = $request->get('status');

        $model = Plasmacenters::query()
            ->orderBy('status', 'desc')
            ->orderBy('created_at', 'desc')
            ->where(function ($q) use ($status) {
                if ($status != '') {
                    $q->where('status', $status);
                }
            })
            ->paginate(20);

        return view('admin.plasmacenters.index', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Plasmacenters();

        $model->is_share_block = 1;
        $model->is_sign_up_block = 1;

        $localizations = Langs::getLangsWithTitle();

        return view('admin.plasmacenters.create', [
            'model'         => $model,
            'localizations' => $localizations,
            'socials' => []
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $model = new Plasmacenters();

        DB::beginTransaction();

        try {
            if (empty($request->input('slug', ''))) {
                $request->merge(array('slug' => SlugService::createSlug(Plasmacenters::class, 'slug', $request->input('page_data.' . Langs::getDefaultLangCode() . '.title'))));
            }

            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;
            $model->is_share_block = $request->get('is_share_block') ?? false;
            $model->is_sign_up_block = $request->get('is_sign_up_block') ?? false;

            if ($request->has('socials')) {
                $model->socials = json_encode(array_values($request->get('socials')));
            } else {
                $model->socials = json_encode([]);
            }

            if (!$model->save()) {
                DB::rollBack();
            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []), $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('plasmacenters.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('plasmacenters.index')->with('success', 'Плазмацентр успешно добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Donors $donor
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Plasmacenters $plasmacenter)
    {
        $localizations = Langs::getLangsWithTitle();

        $socials = json_decode($plasmacenter->socials,true);

        return view('admin.plasmacenters.edit', [
            'model'         => $plasmacenter,
            'data'          => $plasmacenter->getTranslationsArray(),
            'localizations' => $localizations,
            'socials' => $socials
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        /* @var $model Plasmacenters */
        $model = Plasmacenters::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;
            $model->is_share_block = $request->get('is_share_block') ?? false;
            $model->is_sign_up_block = $request->get('is_sign_up_block') ?? false;

            if ($request->has('socials')) {
                $model->socials = json_encode(array_values($request->get('socials')));
            } else {
                $model->socials = json_encode([]);
            }

            if (!$model->save()) {
                DB::rollBack();
            }

            $model->deleteTranslations();

            $model->audio()->sync($request->get('audio'));

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []), $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('plasmacenters.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->back()->with('success', 'Плазмацентр успешно обновлен!');
    }

    /**
     * @param Donors $donor
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Plasmacenters $plasmacenter)
    {
        $plasmacenter->deleteTranslations();
        $plasmacenter->audio()->sync([]);
        $plasmacenter->delete();

        return redirect()->back()->with('success', 'Плазмацентр успешно удален!');
    }

    public function addSocial(Request $request)
    {
        return View::make('admin.plasmacenters._social_elem', [
            'link'    => '',
            'icon'    => '',
            'randKey' => rand(1000, 9999)
        ]);
    }
}
