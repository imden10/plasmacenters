<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BlogArticles;
use App\Models\Donors;
use App\Models\Landing;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Langs;

class DonorsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $status = $request->get('status');

        $model = Donors::query()
            ->orderBy('status', 'desc')
            ->orderBy('created_at', 'desc')
            ->where(function ($q) use ($status) {
                if ($status != '') {
                    $q->where('status', $status);
                }
            })
            ->paginate(20);

        return view('admin.donors.index', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Donors();

        $model->donations = 1;
        $model->is_share_block = 1;
        $model->is_sign_up_block = 1;

        $localizations = Langs::getLangsWithTitle();

        return view('admin.donors.create', [
            'model'         => $model,
            'localizations' => $localizations
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $model = new Donors();

        DB::beginTransaction();

        try {
            if (empty($request->input('slug', ''))) {
                $request->merge(array('slug' => SlugService::createSlug(Donors::class, 'slug', $request->input('page_data.' . Langs::getDefaultLangCode() . '.title'))));
            }

            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;
            $model->is_share_block = $request->get('is_share_block') ?? false;
            $model->is_sign_up_block = $request->get('is_sign_up_block') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []), $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('donors.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('donors.index')->with('success', 'Донора успешно добавлено!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Donors $donor
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(donors $donor)
    {
        $localizations = Langs::getLangsWithTitle();

        return view('admin.donors.edit', [
            'model'         => $donor,
            'data'          => $donor->getTranslationsArray(),
            'localizations' => $localizations
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        /* @var $model Donors */
        $model = Donors::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;
            $model->is_share_block = $request->get('is_share_block') ?? false;
            $model->is_sign_up_block = $request->get('is_sign_up_block') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            $model->deleteTranslations();

            $model->audio()->sync($request->get('audio'));

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []), $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('donors.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->back()->with('success', 'Донора успешно обновлено!');
    }

    /**
     * @param Donors $donor
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Donors $donor)
    {
        $donor->deleteTranslations();
        $donor->audio()->sync([]);
        $donor->delete();

        return redirect()->back()->with('success', 'Донора успешно удалено!');
    }
}
