<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Langs;
use App\Models\Menu;
use App\Models\Translations\MenuTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $tag = $request->get('tag');

        $tags = Menu::getTags();

        if (!$tag) {
            $tag = count($tags) ? array_shift($tags) : null;
        }

        $model = Menu::query()
            ->where('tag', $tag)
            ->where('const', '<>', 1)
            ->defaultOrder()
            ->get()
            ->toTree();

        return view('admin.menu.index', [
            'model' => json_encode($model, JSON_UNESCAPED_UNICODE),
            'tag'   => $tag
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Menu();

        return view('admin.menu.form', [
            'model' => $model
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = $request->all();

        Menu::create($post);

        return redirect()->route('menu.index')->with('success', 'Пункт меню успішно створено!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Menu::query()->where('id', $id)->first();

        $localizations = Langs::getLangsWithTitle();

        return view('admin.menu.form', [
            'model'         => $model,
            'data'          => $model->getTranslationsArray(),
            'localizations' => $localizations,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* @var $model Menu */
        $model = Menu::query()->where('id', $id)->first();

        $post = $request->all();

        $model->fill($post);
        $model->save();

        $model->deleteTranslations();

        foreach (Langs::getLangsWithTitle() as $lang => $item) {
            $model->translateOrNew($lang)->fill($request->input('page_data.' . $lang, []));
            $model->save();
        }

        return redirect()->route('menu.index',['tag' => $model->tag])->with('success', 'Пункт меню успішно оновлено!');
    }

    /**
     * @param Request $request
     * @param $id
     * @return array
     */
    public function destroy(Request $request, $id)
    {
        $confirm = $request->get('c') ?? 1;

        $node = Menu::query()->where('id', $id)->with('children')->first();

        $tag = $node->tag;

        if ($confirm && count($node->children)) {
            return [
                'status'      => false,
                'hasChildren' => true,
                'message'     => 'Цей пункт меню містить в собі інші пункти, які теж будуть видалені!'
            ];
        }

        $node->deleteTranslations();

        $node->delete();

        $tree = Menu::query()
            ->where('tag', $tag)
            ->where('const', '<>', 1)
            ->defaultOrder()
            ->get()
            ->toTree();

        return [
            'status'  => true,
            'tree'    => $tree,
            'message' => 'Пункт меню успішно видалено!'
        ];
    }

    private function treeGetIds($tree,$ids = [])
    {
        foreach ($tree as $item){
            $ids[] = $item['id'];

            if(count($item['children'])){
                $ids = array_merge($ids,$this->treeGetIds($item['children'],$ids));
            }
        }

        return $ids;
    }

    public function rebuild(Request $request)
    {
        $ids = $this->treeGetIds($request->get('tree'));
        $ids = array_unique($ids);

        $tree = Menu::query()
            ->whereNotIn('id',$ids)
            ->defaultOrder()
            ->get()
            ->toTree()
            ->toArray();

        $isRebuild = Menu::rebuildTree(array_merge($tree,$request->get('tree')));

        if ($isRebuild) {
            return [
                'status'  => true,
                'message' => 'Меню успішно оновлено!'
            ];
        }
    }

    public function addMenu(Request $request)
    {
        $post = $request->all();

        Menu::create($post);

        return redirect()->route('menu.index', ['tag' => $request->get('tag')])->with('success', 'Меню успішно створено!');
    }

    public function addItem(Request $request)
    {
        $currentTree = json_decode($request->get('tree'),true);

        $ids = $this->treeGetIds($currentTree);
        $ids = array_unique($ids);

        $tree = Menu::query()
            ->whereNotIn('id',$ids)
            ->defaultOrder()
            ->get()
            ->toTree()
            ->toArray();

        $currentTree[] = $request->except(['_token', 'name', 'url','tree']);

        $isRebuild = Menu::rebuildTree(array_merge($tree,$currentTree));


//        $parent = Menu::query()
//            ->where('tag', $request->get('tag'))
//            ->where('const', '<>', 1)
//            ->defaultOrder()
//            ->get()
//            ->toTree()
//            ->toArray();
//
//        $parent[] = $request->except(['_token', 'name', 'url']);
//
//        $isRebuild = Menu::rebuildTree($parent);

        $model = Menu::query()
            ->where('tag', $request->get('tag'))
            ->orderBy('id', 'desc')
            ->first();

        $modelTrans          = new MenuTranslation();
        $modelTrans->menu_id = $model->id;
        $modelTrans->name    = $request->get('name');
        $modelTrans->url     = $request->get('url') ?? null;
        $modelTrans->lang    = Langs::getDefaultLangCode();
        $modelTrans->save();

        return redirect()->back()->with('success', 'Пункт меню успішно створено!');
    }

    public function deleteMenu(Request $request)
    {
        $menuIds =  Menu::query()
            ->where('tag', $request->get('tag'))
            ->where('const',0)
            ->pluck('id')
            ->toArray();

        Menu::query()
            ->where('tag', $request->get('tag'))
            ->delete();

        MenuTranslation::query()
            ->whereIn('menu_id',$menuIds)
            ->delete();

        return redirect()->route('menu.index')->with('success', 'Меню успішног видалено!');
    }

    public function move(Request $request)
    {
        $action = $request->get('action');
        $node   = Menu::query()->where('id', $request->get('id'))->first();
        $node->$action();

        $tree = Menu::query()
            ->where('tag', $node->tag)
            ->where('const', '<>', 1)
            ->defaultOrder()
            ->get()
            ->toTree();

        return [
            'status'  => true,
            'tree'    => $tree,
            'message' => ''
        ];
    }
}
