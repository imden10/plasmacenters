<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogArticleRequest;
use App\Models\BlogArticleCategory;
use App\Models\BlogArticles;
use App\Models\BlogArticleTag;
use App\Models\Langs;
use App\Models\Services;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status = $request->get('status');
        $name = $request->get('name');

        $model = BlogArticles::query()
            ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
            ->where('blog_article_translations.lang',Langs::getDefaultLangCode())
            ->select([
                'blog_articles.*',
                'blog_article_translations.name'
            ])
            ->orderBy('blog_articles.status', 'desc')
            ->orderBy('blog_articles.created_at', 'desc')
            ->where(function ($q) use ($status,$name) {
                if ($status != '') {
                    $q->where('blog_articles.status', $status);
                }
                if ($name != '') {
                    $q->where('blog_article_translations.name', 'like', '%' . $name . '%');
                }
            })
            ->paginate(50);

        return view('admin.blog.articles.index',[
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new BlogArticles();

        $model->is_share_block = 1;
        $model->is_sign_up_block = 1;

        $localizations = Langs::getLangsWithTitle();

        return view('admin.blog.articles.create', [
            'model'         => $model,
            'localizations' => $localizations
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogArticleRequest $request)
    {
        $model = new BlogArticles();

        $post['tags'] = $request->get('tags') ?? [];

        DB::beginTransaction();

        try {
            if (empty($request->input('slug', ''))) {
                $request->merge(array('slug' => SlugService::createSlug(BlogArticles::class, 'slug', $request->input('page_data.' . Langs::getDefaultLangCode() . '.name'))));
            }

            $request->merge(['public_date' => Carbon::parse($request->get('public_date'))->format('Y-m-d H:i:00')]);

            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;
            $model->is_share_block = $request->get('is_share_block') ?? false;
            $model->is_sign_up_block = $request->get('is_sign_up_block') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            if(! empty($post['tags']) && count($post['tags'])){
                $model->tags()->sync($post['tags']);
            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $data = $request->input('page_data.' . $lang, []);

                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($data,$constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->back()->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('articles.index')->with('success', 'Публикация успешно добавлена!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param BlogArticles $blogArticle
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $blogArticle = BlogArticles::query()->where('id',$id)->first();

        $localizations = Langs::getLangsWithTitle();

        $blogArticle->public_date = Carbon::create($blogArticle->public_date)->format('d-m-Y H:i');

        return view('admin.blog.articles.edit', [
            'model'         => $blogArticle,
            'data'          => $blogArticle->getTranslationsArray(),
            'localizations' => $localizations
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post['tags'] = $request->get('tags') ?? [];

        /* @var $model BlogArticles */
        $model = BlogArticles::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $request->merge(['public_date' => Carbon::parse($request->get('public_date'))->format('Y-m-d H:i:00')]);

            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;
            $model->is_share_block = $request->get('is_share_block') ?? false;
            $model->is_sign_up_block = $request->get('is_sign_up_block') ?? false;

            if (! $model->save()) {
                DB::rollBack();
            }

            $model->tags()->sync($post['tags']);

            $model->deleteTranslations();

            $model->audio()->sync($request->get('audio'));

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $data = $request->input('page_data.' . $lang, []);

                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($data,$constructorData[$lang] ?? []));

                if (! $model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('articles.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->back()->with('success', 'Публикация успешно обновлена!');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $blogArticle = BlogArticles::query()->where('id',$id)->first();
        $blogArticle->deleteTranslations();
        $blogArticle->tags()->sync([]);
        $blogArticle->audio()->sync([]);
        BlogArticles::query()->where('id',$id)->delete();

        return redirect()->back()->with('success', 'Публикацию успешно удалено!');
    }
}
