<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\BlogArticles;
use App\Models\Langs;
use App\Modules\Setting\Setting;
use App\Modules\Widgets\Models\Widget;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Adapter;

class BlogArticlesController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getBySlug(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $model = BlogArticles::query()
            ->active()
            ->where('slug', $request->get('slug'))
            ->with('audio.category',function ($q) use($lang){
                $q->leftJoin('audio_category_translations','audio_category_translations.audio_categories_id','=','audio_categories.id')
                    ->where('audio_category_translations.lang',$lang)
                    ->select(['audio_categories.*','audio_category_translations.title AS categoryName']);
            })
            ->first();

        if (!$model)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);


        $data = $this->adapter->prepareModelResults($model, $lang);

        return $this->successResponse($data);
    }

    public function getAll(Request $request)
    {
        $take = (int)app(Setting::class)->get('news_per_page',Langs::getDefaultLangCode());

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $currentPage = $decodedJson['page'] ?? 1;

        $model = BlogArticles::query()
            ->orderBy('created_at','desc')
            ->active()
            ->with('audio.category',function ($q) use($lang){
                $q->leftJoin('audio_category_translations','audio_category_translations.audio_categories_id','=','audio_categories.id')
                    ->where('audio_category_translations.lang',$lang)
                    ->select(['audio_categories.*','audio_category_translations.title AS categoryName']);
            })
            ->take($take)
            ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take))
            ->get();

        $data = $this->adapter->prepareModelsResults($model, $lang);

        $paginate = [
            'total'        => BlogArticles::query()->active()->count(),
            'per_page'     => $take,
            'current_page' => $currentPage,
        ];

        $data['paginate'] = $paginate;

        $widgetSubscribe = Widget::query()->where('instance','blog-subscribe')->where('lang',$lang)->first()->toArray();
        $widgetJoinUs = Widget::query()->where('instance','blog-join-us')->where('lang',$lang)->first()->toArray();

        $data['constructor'] = [
            'subscribe' => isset($widgetSubscribe) ? $widgetSubscribe['data'] : [],
            'join_us' => isset($widgetJoinUs) ? $widgetJoinUs['data'] : []
        ];

        return $this->successResponse($data);
    }
}
