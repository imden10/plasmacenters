<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\Donors;
use App\Models\Landing;
use App\Models\Langs;
use App\Models\Pages;
use App\Models\PagesMenu;
use App\Models\Plasmacenters;
use App\Modules\Setting\Setting;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Adapter;

class PlasmacentersController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getBySlug(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $model = Plasmacenters::query()
            ->active()
            ->where('slug', $request->get('slug'))
            ->with('audio.category',function ($q) use($lang){
                $q->leftJoin('audio_category_translations','audio_category_translations.audio_categories_id','=','audio_categories.id')
                    ->where('audio_category_translations.lang',$lang)
                    ->select(['audio_categories.*','audio_category_translations.title AS categoryName']);
            })
            ->first();

        if (!$model)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);

        $data = $this->adapter->prepareModelResults($model,$lang);

        return $this->successResponse($data);
    }

    public function getAll(Request $request)
    {
        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $model = Plasmacenters::query()
            ->active()
            ->with('audio.category',function ($q) use($lang){
                $q->leftJoin('audio_category_translations','audio_category_translations.audio_categories_id','=','audio_categories.id')
                    ->where('audio_category_translations.lang',$lang)
                    ->select(['audio_categories.*','audio_category_translations.title AS categoryName']);
            })
            ->get();

        $data = $this->adapter->prepareModelsResults($model,$lang);

        $mainMenuTag = \App\Models\Menu::query()
            ->where('id',app(Setting::class)->get('plasmacenters_menu',\App\Models\Langs::getDefaultLangCode()))
            ->where('const',  1)
            ->first();

        $menu = [];

        if($mainMenuTag){
            $menu = \App\Models\Menu::query()
                ->where('tag', $mainMenuTag->tag)
                ->where('const', '<>', 1)
                ->with(['page','blog','landing','donor','plasmacentr'])
                ->defaultOrder()
                ->get()
                ->toTree()
                ->toArray();
        }

        $data['menu'] = count($menu) ? $this->adapter->prepareMenuResults($menu,$lang) : [];

        return $this->successResponse($data);
    }
}
