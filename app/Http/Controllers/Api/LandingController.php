<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\Landing;
use App\Models\Langs;
use App\Models\Pages;
use App\Models\PagesMenu;
use App\Modules\Setting\Setting;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Adapter;

class LandingController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getBySlug(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $model = Landing::query()
            ->active()
            ->where('slug', $request->get('slug'))
            ->with('audio.category',function ($q) use($lang){
                $q->leftJoin('audio_category_translations','audio_category_translations.audio_categories_id','=','audio_categories.id')
                    ->where('audio_category_translations.lang',$lang)
                    ->select(['audio_categories.*','audio_category_translations.title AS categoryName']);
            })
            ->first();

        if (!$model)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);


        $data = $this->adapter->prepareModelResults($model,$lang);

        /* landing 1 */
        if($model->id == 5){
            $menuId = 250;
        }
        /* landing 2 */
        elseif($model->id == 6){
            $menuId = 251;
        }

        if(isset($menuId)){
            $mainMenuTag = \App\Models\Menu::query()
                ->where('id',$menuId)
                ->where('const',  1)
                ->first();

            $menu = [];

            if($mainMenuTag){
                $menu = \App\Models\Menu::query()
                    ->where('tag', $mainMenuTag->tag)
                    ->where('const', '<>', 1)
                    ->with(['page','blog','landing','donor','plasmacentr'])
                    ->defaultOrder()
                    ->get()
                    ->toTree()
                    ->toArray();
            }

            $data['menu'] = count($menu) ? $this->adapter->prepareMenuResults($menu,$lang) : [];
        }

        return $this->successResponse($data);
    }
}
