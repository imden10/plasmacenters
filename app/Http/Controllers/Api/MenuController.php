<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\Langs;
use App\Models\Pages;
use App\Service\Adapter;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MenuController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getByName(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['name'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['name']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $model = \App\Models\Menu::query()
            ->where('tag', $decodedJson['name'])
            ->where('const', '<>', 1)
            ->with(['page','blog','landing','donor','plasmacentr'])
            ->defaultOrder()
            ->get()
            ->toTree()
            ->toArray();

        if (!$model)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);



        $data = $this->adapter->prepareMenuResults($model,$lang);

        return $this->successResponse($data);
    }

    public function getById(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['id'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['id']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $main = \App\Models\Menu::query()
            ->where('id',$decodedJson['id'])
            ->where('const',  1)
            ->first();

        if(! $main){
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);
        }

        $model = \App\Models\Menu::query()
            ->where('tag', $main->tag)
            ->where('const', '<>', 1)
            ->with(['page','blog','landing','donor','plasmacentr'])
            ->defaultOrder()
            ->get()
            ->toTree()
            ->toArray();

        if (!$model)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);



        $data = $this->adapter->prepareMenuResults($model,$lang);

        return $this->successResponse($data);
    }

    public function getByIds(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['ids'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['ids']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $res = [];

        foreach ($decodedJson['ids'] as $id){
            $main = \App\Models\Menu::query()
                ->where('id',$id)
                ->where('const',  1)
                ->first();

            if(! $main){
                return $this->errorResponse(
                    ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                    Response::HTTP_NOT_FOUND);
            }

            $model = \App\Models\Menu::query()
                ->where('tag', $main->tag)
                ->where('const', '<>', 1)
                ->with(['page','blog','landing','donor','plasmacentr'])
                ->defaultOrder()
                ->get()
                ->toTree()
                ->toArray();

            $res[$id]['name'] = $main->tag;

            if (!$model)
                return $this->errorResponse(
                    ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                    Response::HTTP_NOT_FOUND);



            $res[$id]['items'] = $this->adapter->prepareMenuResults($model,$lang);
        }

        return $this->successResponse($res);
    }

}
