<?php


namespace App\Service;


use App\Core\Error\ErrorManager;
use App\Models\Audio;
use App\Models\Donors;
use App\Models\Faq;
use App\Models\Menu;
use App\Models\Pages;
use App\Models\Translations\AudioCategoryTranslation;
use App\Models\Translations\AudioTranslation;
use App\Modules\Widgets\Models\Widget;
use Dompdf\Exception;
use Symfony\Component\HttpFoundation\Response;

class Adapter
{
    private static function cmp($a, $b)
    {
        return $a['position'] - $b['position'];
    }

    /**
     * @param $model
     * @param $lang
     * @return array
     */
    public function prepareModelResults($model, $lang): array
    {
        $translate   = $model->getTranslation($lang);
        $constructor = $model->getTranslation($lang)->constructor->data;

        $model = $model->toArray();

        if (isset($model['menu_id']) && !is_null($model['menu_id'])) {
            $main = \App\Models\Menu::query()
                ->where('id', $model['menu_id'])
                ->where('const', 1)
                ->first();

            if ($main) {
                $modelMenu = \App\Models\Menu::query()
                    ->where('tag', $main->tag)
                    ->where('const', '<>', 1)
                    ->with(['page', 'blog', 'landing', 'donor', 'plasmacentr'])
                    ->defaultOrder()
                    ->get()
                    ->toTree()
                    ->toArray();

                if ($modelMenu) {
                    $model['menu_items'] = $this->prepareMenuResults($modelMenu, $lang);
                }
            }
        }

        $audio = [];

        if (count($model['audio'])) {
            foreach ($model['audio'] as $item) {
                foreach ($item['translations'] as $trans) {
                    if ($trans['lang'] === $lang) {
                        $audio[] = [
                            'category' => $item['category']['categoryName'] ?? '',
                            'src'      => $trans['url'],
                            'name'     => $trans['name']
                        ];
                    }
                }
            }
        }

        $socials = [];

        if (isset($model['socials']) && count(json_decode($model['socials'], true))) {
            $socials = json_decode($model['socials'], true);
        }

        unset($model['translations']);
        unset($model['audio']);
        unset($model['socials']);
        unset($translate['constructor']);
        unset($model['menu_id']);

        if (!is_array(current($constructor)))
            $constructor = [];

        if (count($constructor)) {
            usort($constructor, array('App\Service\Adapter', 'cmp'));

            $allWidgets     = Widget::query()->where('lang', $lang)->pluck('data', 'id')->toArray();
            $allWidgetsName = Widget::query()->where('lang', $lang)->pluck('instance', 'id')->toArray();

            foreach ($constructor as $key => $item) {
                if ($item['component'] === 'widget') {
                    if (isset($allWidgetsName[$item['content']['widget']])) {
                        $constructor[$key]['content']['instance'] = $allWidgetsName[$item['content']['widget']];
                        $widgetClassName                          = config('widgets.widgets')[$allWidgetsName[$item['content']['widget']]];
                        $widgetClass                              = app($widgetClassName);

                        if (method_exists($widgetClass, 'adapter')) {
                            $constructor[$key]['content']['data'] = $widgetClass->adapter($allWidgets[$item['content']['widget']], $lang);
                        } else {
                            $constructor[$key]['content']['data'] = $allWidgets[$item['content']['widget']];
                        }

                        $constructor[$key]['component'] = $constructor[$key]['content']['instance'];
                        $constructor[$key]['content']   = $constructor[$key]['content']['data'];

                        if ($constructor[$key]['component'] === 'menu') {
                            $constructor[$key]['component'] = 'menucomp';
                            unset($constructor[$key]['menu']);
                        }

                        if ($constructor[$key]['component'] === 'banner-new-landing') {
                            $list123 = [];

                            if(isset($constructor[$key]['content']['list_default']) && count($constructor[$key]['content']['list_default'])){
                                foreach ($constructor[$key]['content']['list_default'] as $itm) {
                                    $list123[] = $itm['item'];
                                }

                                $constructor[$key]['content']['list_default'] = $list123;
                            }
                        }

                    } else {
                        unset($constructor[$key]);
                    }

                } elseif ($item['component'] === 'donors') {
                    if (count($item['content']['list'])) {
                        $list = [];
                        $ids  = [];

                        foreach ($item['content']['list'] as $l) {
                            $ids[] = (int)$l['donor_id'];
                        }

                        if (count($ids)) {
                            $donors = Donors::query()
                                ->whereIn('id', $ids)
                                ->get()
                                ->toArray();

                            if (count($donors)) {
                                foreach ($donors as $key2 => $item2) {
                                    $url = \App\Models\Menu::getTypesModel()[\App\Models\Menu::TYPE_DONOR]['url_prefix'] . $item2['slug'];

                                    if ($lang !== \App\Models\Langs::getDefaultLangCode()) {
                                        $url = '/' . $lang . '/' . $url;
                                    } else {
                                        $url = '/' . $url;
                                    }

                                    $list[$key2]['url'] = $url;

                                    foreach ($item2['translations'] as $trans) {
                                        if ($trans['lang'] === $lang) {
                                            $list[$key2]['title'] = $trans['title'];
                                            $list[$key2]['city']  = $trans['city'];
                                        }
                                    }

                                    $list[$key2]['image']     = $item2['image'];
                                    $list[$key2]['donations'] = $item2['donations'];
                                }

                                $constructor[$key]['content']['list'] = $list;
                            }
                        }
                    }
                } elseif ($item['component'] === 'see-also') {
                    $list = [];
                    $ids  = [];

                    foreach ($item['content']['list'] as $l) {
                        $ids[] = (int)$l['page_id'];
                    }

                    if (count($ids)) {
                        $pages = Pages::query()
                            ->whereIn('id', $ids)
                            ->get()
                            ->toArray();

                        if (count($pages)) {
                            foreach ($pages as $pageKey => $page) {
                                $url = \App\Models\Menu::getTypesModel()[\App\Models\Menu::TYPE_PAGE]['url_prefix'] . $page['slug'];

                                if ($lang !== \App\Models\Langs::getDefaultLangCode()) {
                                    $url = '/' . $lang . '/' . $url;
                                } else {
                                    $url = '/' . $url;
                                }

                                $list[$pageKey]['url'] = $url;

                                foreach ($page['translations'] as $trans) {
                                    if ($trans['lang'] === $lang) {
                                        $list[$pageKey]['title'] = $trans['title'];
                                    }
                                }
                            }

                            $constructor[$key]['content']['list'] = $list;
                        }
                    }
                } elseif ($item['component'] === 'question-answer') {
                    $list = [];
                    $ids  = [];

                    foreach ($item['content']['list'] as $l) {
                        $ids[] = (int)$l['faq_id'];
                    }

                    if (count($ids)) {
                        $faq = Faq::query()
                            ->whereIn('id', $ids)
                            ->get()
                            ->toArray();

                        if (count($faq)) {
                            foreach ($faq as $faqKey => $faqItem) {
                                foreach ($faqItem['translations'] as $trans) {
                                    if ($trans['lang'] === $lang) {
                                        $list[$faqKey]['title'] = $trans['question'];
                                        $list[$faqKey]['text']  = $trans['answer'];
                                    }
                                }
                            }

                            $constructor[$key]['content']['list'] = $list;
                        }
                    }
                }
            }
        }

        return [
            'model'       => $model,
            'translate'   => $translate,
            'audio'       => $audio,
            'socials'     => $socials,
            'constructor' => array_values($constructor)
        ];
    }

    /**
     * @param $model
     * @param $lang
     * @return array
     */
    public function prepareModelsResults($model, $lang): array
    {
        $models = [];

        foreach ($model as $item) {
            $models[] = $this->prepareModelResults($item, $lang);
        }

        return [
            'models' => $models
        ];
    }

    public function prepareMenuResults($model, $lang): array
    {
        $res = [];

        $res = $this->getChildrenMenu($model, $lang, $res);

        return $res;
    }

    public function getChildrenMenu($children = null, $lang, $res = [])
    {
        foreach ($children as $key => $item) {
            if ($item['type'] == Menu::TYPE_ARBITRARY) {
                $res[$key]['name'] = $item['name'];
                $res[$key]['type'] = $item['type'];
                $res[$key]['icon'] = $item['icon'];

                foreach ($item['translations'] as $trans) {
                    if ($trans['lang'] === $lang) {
                        $res[$key]['name'] = $trans['name'];
                        $res[$key]['url']  = $trans['url'];
                        $res[$key]['slug'] = null;
                    }
                }
            } else {
                $modelRel = $item[\App\Models\Menu::getTypesModel()[$item['type']]['rel']];

                if (is_null($modelRel)) {
                    continue;
                }

                $res[$key]['url'] = \App\Models\Menu::getTypesModel()[$item['type']]['url_prefix'] . $modelRel['slug'];
                if ($lang !== \App\Models\Langs::getDefaultLangCode()) {
                    $res[$key]['url'] = '/' . $lang . '/' . $res[$key]['url'];
                } else {
                    $res[$key]['url'] = '/' . $res[$key]['url'];
                }

                $res[$key]['name'] = $modelRel[\App\Models\Menu::getTypesModel()[$item['type']]['name']];
                $res[$key]['type'] = $item['type'];
                $res[$key]['icon'] = $item['icon'];

                foreach ($modelRel['translations'] as $trans) {
                    if ($trans['lang'] === $lang) {
                        $res[$key]['name'] = $trans[\App\Models\Menu::getTypesModel()[$item['type']]['name']];
                        $res[$key]['slug'] = $modelRel['slug'];
                    }
                }

                foreach ($item['translations'] as $trans) {
                    if ($trans['lang'] === $lang) {
                        if (!empty($trans['name'])) {
                            $res[$key]['name'] = $trans['name'];
                        }
                    }
                }
            }

            if (count($item['children'])) {
                $res[$key]['children'] = $this->getChildrenMenu($item['children'], $lang, []);
            }
        }

        return $res;
    }

    public function prepareFaqsResults($model, $lang): array
    {
        $data = [];

        foreach ($model as $key => $item) {
            $elem = [];

            $audio = AudioTranslation::query()
                ->where('audio_id', $item->audio_id)
                ->where('lang', $lang)
                ->first();

            $elem['audio_category'] = '';

            if (isset($audio->url)) {
                $a = Audio::query()
                    ->where('audio.id', $item->audio_id)
                    ->with('category', function ($q) use ($lang) {
                        $q->leftJoin('audio_category_translations', 'audio_category_translations.audio_categories_id', '=', 'audio_categories.id')
                            ->where('audio_category_translations.lang', $lang)
                            ->select(['audio_categories.*', 'audio_category_translations.title AS categoryName']);
                    })
                    ->first();

                if (isset($a['category']['categoryName'])) {
                    $elem['audio_category'] = $a['category']['categoryName'] ?? '';
                }
            }

            $elem['audio'] = $audio->url ?? null;
            $translate     = $item->getTranslation($lang);

            $elem['question'] = $translate->question;
            $elem['answer']   = $translate->answer;

            $data[] = $elem;
        }

        return $data;
    }
}
