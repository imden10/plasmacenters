<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Audio
 * @package App\Models
 *
 * @property AudioCategories $category
 */
class Audio extends Model
{
    use HasFactory;
    use Translatable;

    protected $table = 'audio';

    public $translationModel = 'App\Models\Translations\AudioTranslation';

    public $translatedAttributes = [
        'name',
        'url',
    ];

    protected $fillable = ['category_id'];

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->name;
    }

    public function category()
    {
        return $this->hasOne(AudioCategories::class,'id','category_id');
    }
}
