<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AudioCategories extends Model
{
    use HasFactory;

    use Translatable;

    protected $table = 'audio_categories';

    public $translationModel = 'App\Models\Translations\AudioCategoryTranslation';

    public $translatedAttributes = [
        'title'
    ];

    protected $fillable = ['id'];

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->title;
    }
}
