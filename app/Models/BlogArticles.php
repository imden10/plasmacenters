<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BlogArticles
 * @package App\Models
 *
 * @property integer $id
 * @property string $slug
 * @property integer $status
 * @property integer $views
 * @property integer $user_id
 * @property Carbon $public_date
 * @property string $image
 * @property string $youtube
 * @property integer $is_share_block
 * @property integer $is_sign_up_block
 * @property integer $menu_id
 *
 * @property BlogTags[] $tags
 * @property Audio $audio
 */
class BlogArticles extends Model
{
    use HasFactory;
    use Sluggable;
    use Translatable;

    protected $table = 'blog_articles';

    public $translationModel = 'App\Models\Translations\BlogArticleTranslation';

    public $translatedAttributes = [
        'name',
        'excerpt',
        'text',
        'meta_title',
        'meta_keywords',
        'meta_description',
    ];

    protected $fillable = [
        'slug',
        'views',
        'user_id',
        'image',
        'public_date',
        'youtube',
        'menu_id',
        'is_share_block',
        'is_sign_up_block'
    ];

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE     = 1;

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->name;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'defaultTitle'
            ]
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => 'Не активна',
            self::STATUS_ACTIVE     => 'Активна'
        ];
    }

    /**
     * @return array
     */
    public static function getStatusColors(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => ['#ff3838', '#fdfdfd'],
            self::STATUS_ACTIVE     => ['#49cc00', 'white']
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(BlogTags::class, 'blog_article_tag', 'blog_article_id', 'blog_tag_id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE)
            ->where('public_date', '<=', Carbon::now());
    }

    public function audio()
    {
        return $this->belongsToMany(Audio::class);
    }
}
