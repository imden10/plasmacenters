<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AudioBlogArticles extends Model
{
    use HasFactory;

    protected $table = 'audio_blog_articles';

    public $timestamps = false;

    protected $guarded = [];
}
