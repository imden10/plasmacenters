<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AudioPlasmacenters extends Model
{
    use HasFactory;

    protected $table = 'audio_plasmacenters';

    public $timestamps = false;

    protected $guarded = [];
}
