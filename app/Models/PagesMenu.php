<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PagesMenu
 * @package App\Models
 *
 * @property integer $page_id
 * @property string $model_type
 * @property integer $model_id
 * @property integer $order
 * @property string $model_title
 * @property string $model_slug
 */
class PagesMenu extends Model
{
    use HasFactory;

    protected $table = 'pages_menus';

    protected $fillable = [
        'page_id',
        'model_type',
        'model_id',
        'order',
        'model_title',
        'model_slug'
    ];
}
