<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class AudioPages extends Pivot
{
    use HasFactory;

    protected $table = 'audio_pages';

    public $timestamps = false;

    protected $guarded = [];
}
