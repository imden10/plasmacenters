<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AudioTranslation extends Model
{
    use HasFactory;

    protected $table = 'audio_translations';

    protected $fillable = [
        'name',
        'url',
    ];

    public $timestamps = false;

    private string $entityAttribute = 'audio_id';
}
