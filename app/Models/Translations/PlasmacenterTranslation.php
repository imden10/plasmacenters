<?php

namespace App\Models\Translations;

use App\Modules\Constructor\Collections\ComponentCollections;
use App\Modules\Constructor\Contracts\HasConstructor;
use App\Modules\Constructor\Traits\Constructorable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlasmacenterTranslation extends Model implements HasConstructor
{
    use HasFactory;
    use Constructorable;

    protected $table = 'plasmacenter_translations';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'city',
        'address',
        'schedule',
        'email',
        'phone',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    private string $entityAttribute = 'plasmacenters_id';

    /**
     * @param array $attributes
     * @return Model
     */
    public function fill(array $attributes = [])
    {
        $this->fillConstructorable($attributes);

        return parent::fill($attributes);
    }

    /**
     * @inheritDoc
     */
    public function constructorComponents(): array
    {
        return [
            'simple-text'     => ComponentCollections::simpleText(),
            //            'image-and-text' => ComponentCollections::imageAndText(),
            'list'            => ComponentCollections::list(),
            'gallery'         => ComponentCollections::gallery(),
            'image-element'   => ComponentCollections::imageElement(),
            'text-2-columns'  => ComponentCollections::text2Columns(),
            'question-answer' => ComponentCollections::questionAnswer(),
            'button'          => ComponentCollections::button(),
            'donors'          => ComponentCollections::donors(),
            'see-also'        => ComponentCollections::seeAlso(),
            'widget'          => ComponentCollections::widget(),
        ];
    }
}
