<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AudioCategoryTranslation extends Model
{
    use HasFactory;

    protected $table = 'audio_category_translations';

    private string $entityAttribute = 'audio_categories_id';

    public $timestamps = false;

    protected $fillable = [
        'title'
    ];
}
