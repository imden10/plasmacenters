<?php

namespace App\Models;

use App\Modules\Constructor\Collections\ComponentCollections;
use App\Modules\Constructor\Traits\Constructorable;
use Astrotomic\Translatable\Translatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Modules\Constructor\Contracts\HasConstructor;

/**
 * Class Pages
 * @package App
 *
 * @property string $slug
 * @property integer $status
 * @property string $youtube
 * @property integer $menu_id
 * @property integer $is_share_block
 * @property integer $is_sign_up_block
 * @property string $image
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property array $getStatuses
 * @property Audio $audio
 */
class Pages extends Model
{
    use Sluggable;
    use Translatable;

    protected $table = 'pages';

    public $translationModel = 'App\Models\Translations\PagesTranslation';

    public $translatedAttributes = [
        'title',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    protected $fillable = [
        'slug',
        'status',
        'youtube',
        'menu_id',
        'image',
        'is_share_block',
        'is_sign_up_block'
    ];

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE     = 1;

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->title;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => [
                'title'    => 'Не активная',
                'bg_color' => '#ff3838',
                'color'    => '#fdfdfd'
            ],
            self::STATUS_ACTIVE     => [
                'title'    => 'Активная',
                'bg_color' => '#49cc00',
                'color'    => 'white'
            ]
        ];
    }

    public function showStatus()
    {
        return '<span class="badge" style="color: '.self::getStatuses()[$this->status]["color"].'; background-color: '.self::getStatuses()[$this->status]["bg_color"].'">'.self::getStatuses()[$this->status]["title"].'</span>';
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function audio()
    {
        return $this->belongsToMany(Audio::class);
    }
}
