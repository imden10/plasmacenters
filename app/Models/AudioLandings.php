<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AudioLandings extends Model
{
    use HasFactory;

    protected $table = 'audio_landing';

    public $timestamps = false;

    protected $guarded = [];
}
