<?php

namespace App\Models;

use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class UploadFile
{
    public function upload(UploadedFile $file, string $path, $sizes = null, $watermark = false): string
    {
        $src = $file->getRealPath();

        $img = Image::make($src);

        $imgName = uniqid(time());

        if (!is_null($sizes)) {
            $sizes = config('image.sizes.' . $sizes);

            $img->backup();

            foreach ((array)$sizes as $name => $size) {

                $width = $size[0] ?? null;

                $height = $size[1] ?? null;

                $img->fit($width, $height);

                if ($watermark && $width >= 500) {
                    /* insert watermark at bottom-right corner with 15px offset */
                    $img->insert(public_path('img/watermark.png'), 'bottom-right', 15, 15);
                }
                elseif ($watermark && $width >= 300) {
                    /* insert watermark at bottom-right corner with 10px offset */
                    $img->insert(public_path('img/watermark-medium.png'), 'bottom-right', 10, 10);
                }

                $img->save(public_path('uploads' . DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR . $imgName . '-' . $name . '.jpg'));
                $img->reset();
            }

            $img->destroy();
        } else {
            $imgName = uniqid(time());
            $img->save(public_path('uploads' . DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR . $imgName . '.jpg'));
        }

        return $imgName . '.jpg';
    }

    public function delete($fileName, string $path, $withSizes = null)
    {
        $arrPath = [];

        if (!$fileName) {
            return;
        }

        if (gettype($fileName) == 'string') {
            $arrPath[] = $fileName;
        } else {
            $arrPath = $fileName;
        }

        if (!is_null($withSizes)) {
            $sizes = config('image.sizes.' . $withSizes);

            foreach ((array)$sizes as $sizeName => $size) {
                foreach ($arrPath as $name) {
                    $parts = pathinfo($name);
                    $file  = public_path('uploads' . DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR . $parts['filename'] . '-' . $sizeName . '.' . $parts['extension']);
                    File::delete($file);
                }
            }
        } else {
            foreach ($arrPath as $name) {
                File::delete(public_path('uploads' . DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR . $name));
            }
        }
    }
}
