<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Plasmacenters extends Model
{
    use Sluggable;
    use Translatable;

    protected $table = 'plasmacenters';

    public $translationModel = 'App\Models\Translations\PlasmacenterTranslation';

    public $translatedAttributes = [
        'title',
        'city',
        'address',
        'schedule',
        'email',
        'phone',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    protected $fillable = [
        'slug',
        'status',
        'coord_lat',
        'coord_lng',
        'socials',
        'menu_id',
        'image',
        'is_share_block',
        'is_sign_up_block',
        'request_link',
        'sort',
        'tag'
    ];

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE     = 1;

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->title;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => 'Не активный',
            self::STATUS_ACTIVE     => 'Активный'
        ];
    }

    /**
     * @return array
     */
    public static function getStatusColors(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => ['#ff3838', '#fdfdfd'],
            self::STATUS_ACTIVE     => ['#49cc00', 'white']
        ];
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function audio()
    {
        return $this->belongsToMany(Audio::class);
    }
}
