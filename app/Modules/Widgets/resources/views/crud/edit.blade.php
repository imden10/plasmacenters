@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item"><a href="/admin/widgets">Віджети</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$widget->instance}}</li>
        </ol>
    </nav>

    <form action="{{ route(config('widgets.route_name_prefix', 'admin.') . 'widgets.update', $widget) }}" method="post">
        @csrf

        @method('put')

        <input type="hidden" name="instance" value="{{ $widget->instance }}">
        <input type="hidden" name="lang" value="{{ request()->get('lang') ?? \App\Models\Langs::getDefaultLangCode() }}">

        <div class="card">
            <div class="card-header">
                <div class="form-row">
                    <div class="form-group input-group-sm col-sm-12">
                        <label for="widgetName">Название виджета</label>

                        <input type="text" name="name" id="widgetName" class="form-control @error('name') is-invalid @enderror" value="{{ $widget->name ?? '' }}">

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="card-body">
                @foreach($object->fields() as $field)
                    @if (isset($field['separator']))
                        @if (!$loop->first)
                            <br /><br />
                        @endif

                        <h5>{{ trans($field['separator']) }}</h5><hr />
                    @else
                        @includeif('widgets::fields.' . $field['type'], ['value' => isset($widget, $widget->data[$field['name']]) ? $widget->data[$field['name']] : $field['value'], 'list' => isset($field['list']) ? $field['list'] : []])
                    @endif
                @endforeach
            </div>
        </div>

        <div class="col-md-12 text-right">
            <button type="submit" class="btn btn-sm btn-info btn-lg">
                <i class="far fa-save"></i>
                Обновить
            </button>
        </div>
    </form>
@endsection
