<div class="form-group">
    <label for="widget{{ studly_case($field['name']) }}">{{ $field['label'] }}</label>

    <div class="input-group mb-3">
        <div style="display: none;">
            <div data-item-id="#dynamicListPlaceholder" class="item-template item-group input-group mb-3 align-items-center border border-grey-light pt-2 pb-2">
                <div class="col-lg-4 mb-4">
                    <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][link]" placeholder="Ссылка" class="form-control mb-1" disabled>
                </div>

                <div class="col-lg-4 mb-4">
                    <div>
                        <select class="select2-field" name="{{ $field['name'] }}[#dynamicListPlaceholder][type]" style="width: 100%">
                            <option value="ic-facebook">Facebook</option>
                            <option value="ic-twitter">Twitter</option>
                            <option value="ic-instagram">Instagram</option>
                            <option value="ic-viber">Viber</option>
                            <option value="ic-youtube">Youtube</option>
                            <option value="ic-telegram">Telegram</option>
                            <option value="ic-phone">Телефон</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg-4">
                    <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                </div>
            </div>
        </div>

        <input type="hidden" name="{{ $field['name'] }}" value="">

        <div class="items-container w-100">
            @foreach((array) old($field['name'], $value) as $key => $value)
                <div data-item-id="{{ $key }}" class="item-template item-group input-group mb-3 align-items-center border border-grey-light pt-2 pb-2">
                    <div class="col-lg-4 mb-4">
                        <input type="text" name="{{ $field['name'] }}[{{ $key }}][link]" value="{{ old($field['name'] . '.' . $key . '.link', $value['link'] ?? '') }}" placeholder="Ссылка" class="form-control mb-1">
                    </div>

                    <div class="col-lg-4 mb-4">
                        <div>
                            <select class="select2-field" name="{{ $field['name'] }}[{{ $key }}][type]" style="width: 100%">
                                <option value="ic-facebook" @if(old($field['name'] . '.' . $key . '.type', $value['type'] ?? '') == 'ic-facebook') selected @endif>Facebook</option>
                                <option value="ic-twitter" @if(old($field['name'] . '.' . $key . '.type', $value['type'] ?? '') == 'ic-twitter') selected @endif>Twitter</option>
                                <option value="ic-instagram" @if(old($field['name'] . '.' . $key . '.type', $value['type'] ?? '') == 'ic-instagram') selected @endif>Instagram</option>
                                <option value="ic-viber" @if(old($field['name'] . '.' . $key . '.type', $value['type'] ?? '') == 'ic-viber') selected @endif>Viber</option>
                                <option value="ic-youtube" @if(old($field['name'] . '.' . $key . '.type', $value['type'] ?? '') == 'ic-youtube') selected @endif>Youtube</option>
                                <option value="ic-telegram" @if(old($field['name'] . '.' . $key . '.type', $value['type'] ?? '') == 'ic-telegram') selected @endif>Telegram</option>
                                <option value="ic-phone" @if(old($field['name'] . '.' . $key . '.type', $value['type'] ?? '') == 'ic-phone') selected @endif>Телефон</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                    </div>

                    @error($field['name'] . '.' . $key)
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            @endforeach
        </div>
    </div>

    <button type="button" class="btn btn-info btn-sm add-item-{{ studly_case($field['name']) }}">Добавить</button>
</div>

@push('styles')
    <link rel="stylesheet" href="{{asset('matrix/libs/select2/dist/css/select2.min.css')}}">
@endpush

@push('scripts')
    <script src="{{asset('matrix/libs/select2/dist/js/select2.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.select2-field-shown').each(function () {
                $(this).select2({});
            });
        });

        $(document).on('click', '.add-item-{{ studly_case($field['name']) }}', function () {
            const parent = $(this).parent();
            const template = parent.find('.item-template');
            const container = parent.find('.items-container');

            create_item(template, container, '#dynamicListPlaceholder');

            container.find('input, textarea').prop('disabled', false);

            container.find('textarea').each(function () {
                if ($(this).hasClass('summernote')) {
                    $(this).summernote(summernote_options);
                }
            });

            container.find('.select2-field').each(function () {
                $(this).select2({});
            });
        });

        $('.items-container').find('textarea').each(function () {
            if ($(this).hasClass('summernote')) {
                $(this).summernote(summernote_options);
            }
        });

        $(document).on('click', '.remove-item', function () {
            $(this).parents('.item-group').remove();
        });
    </script>
@endpush
