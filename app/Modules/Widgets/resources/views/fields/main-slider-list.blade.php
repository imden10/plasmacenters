<div class="form-group">
    <label for="widget{{ studly_case($field['name']) }}">{{ $field['label'] }}</label>

    <div class="input-group mb-3">
        <div style="display: none;">
            <div data-item-id="#dynamicListPlaceholder" class="item-template item-group input-group mb-3 align-items-center border border-grey-light pt-2 pb-2">
                <div class="col-md-12 input-group-sm">
                    <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][title]" placeholder="Заголовок" class="form-control mb-1" disabled>
                </div>

                <div class="col-md-12 input-group-sm" style="margin-top: 20px">
                    <div class="input-group color-picker" title="Цвет заголовка">
                        <input type="text" class="form-control input-lg" name="{{ $field['name'] }}[#dynamicListPlaceholder][title_color]" />
                        <span class="input-group-append">
                            <span class="input-group-text colorpicker-input-addon"><i></i></span>
                          </span>
                    </div>
                </div>

                <div class="col-md-12 input-group-sm" style="margin-top: 20px">
                    <textarea name="{{ $field['name'] }}[#dynamicListPlaceholder][text]" id="" cols="30" rows="10" class="form-control" placeholder="Текст"></textarea>
                </div>

                <div class="col-md-12" style="margin-top: 30px">
                    <h4>Кнопка</h4>
                </div>

                <div class="col-md-4 input-group-sm" style="margin-top: 15px">
                    <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][btn_name]" placeholder="Надпись на кнопке" class="form-control mb-1" disabled>
                </div>

                <div class="col-md-4 input-group-sm" style="margin-top: 15px">
                    <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][btn_link]" placeholder="Ссылка на кнопке" class="form-control mb-1" disabled>
                </div>

                <div class="col-md-4 input-group-sm" style="margin-top: 15px">
                    <label>
                        <input type="checkbox" name="{{ $field['name'] }}[#dynamicListPlaceholder][btn_event]" value="1">
                        Форма "Записатися зараз"
                    </label>
                </div>

                <div class="col-md-6" style="margin-top: 30px">
                    <label>Изображение для больших эранов</label>
                    {{ media_preview_box($field['name'] . '[#dynamicListPlaceholder][image]') }}
                </div>

                <div class="col-md-6" style="margin-top: 30px">
                    <label>Изображение для мобильного</label>
                    {{ media_preview_box($field['name'] . '[#dynamicListPlaceholder][image_mob]') }}
                </div>

                <div class="col-md-12">
                    <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                </div>
            </div>
        </div>

        <input type="hidden" name="{{ $field['name'] }}" value="">

        <div class="items-container w-100">
            @foreach((array) old($field['name'], $value) as $key => $value)
                <div data-item-id="{{ $key }}" class="item-template item-group input-group mb-3 align-items-center border border-grey-light pt-2 pb-2">
                    <div class="col-md-12 input-group-sm">
                        <input type="text" name="{{ $field['name'] }}[{{ $key }}][title]" placeholder="Заголовок" value="{{ old($field['name'] . '.' . $key . '.title', $value['title'] ?? '') }}" class="form-control mb-1">
                    </div>

                    <div class="col-md-12 input-group-sm" style="margin-top: 20px">
                        <div class="input-group color-picker-set" title="Using input value">
                            <input type="text" class="form-control input-lg" name="{{ $field['name'] }}[{{ $key }}][title_color]" value="{{ old($field['name'] . '.' . $key . '.title_color', $value['title_color'] ?? '') }}"/>
                            <span class="input-group-append">
                                <span class="input-group-text colorpicker-input-addon"><i></i></span>
                              </span>
                        </div>
                    </div>

                    <div class="col-md-12 input-group-sm" style="margin-top: 20px">
                        <textarea name="{{ $field['name'] }}[{{ $key }}][text]" id="" class="form-control" cols="30" rows="10" placeholder="Текст" class="form-control">{{ old($field['name'] . '.' . $key . '.text', $value['text'] ?? '') }}</textarea>
                    </div>

                    <div class="col-md-12" style="margin-top: 30px">
                        <h4>Кнопка</h4>
                    </div>

                    <div class="col-md-4 input-group-sm" style="margin-top: 15px">
                        <input type="text" name="{{ $field['name'] }}[{{ $key }}][btn_name]" placeholder="Надпись на кнопке" value="{{ old($field['name'] . '.' . $key . '.btn_name', $value['btn_name'] ?? '') }}" class="form-control mb-1">
                    </div>

                    <div class="col-md-4 input-group-sm" style="margin-top: 15px">
                        <input type="text" name="{{ $field['name'] }}[{{ $key }}][btn_link]" placeholder="Ссылка на кнопке" value="{{ old($field['name'] . '.' . $key . '.btn_link', $value['btn_link'] ?? '') }}" class="form-control mb-1">
                    </div>

                    <div class="col-md-4 input-group-sm" style="margin-top: 15px">
                        <label>
                            <?php $v = old($field['name'] . '.' . $key . '.btn_event', $value['btn_event'] ?? '')?>
                            <input type="checkbox" name="{{ $field['name'] }}[{{ $key }}][btn_event]" @if($v) checked @endif>
                            Форма "Записатися зараз"
                        </label>
                    </div>

                    <div class="col-md-6" style="margin-top: 30px">
                        <label>Изображение для больших эранов</label>
                        {{ media_preview_box($field['name'] . '[' . $key . '][image]', $value['image'] ?? null, $errors) }}
                    </div>

                    <div class="col-md-6" style="margin-top: 30px">
                        <label>Изображение для мобильного</label>
                        {{ media_preview_box($field['name'] . '[' . $key . '][image_mob]', $value['image_mob'] ?? null, $errors) }}
                    </div>

                    <div class="col-md-12">
                        <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                    </div>

                    @error($field['name'] . '.' . $key)
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            @endforeach
        </div>
    </div>

    <button type="button" class="btn btn-info btn-sm add-item-{{ studly_case($field['name']) }}">Добавить</button>
</div>

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/3.4.0/css/bootstrap-colorpicker.css">
@endpush

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/3.4.0/js/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            // $('.color-picker-set').colorpicker({});
        });

        $(document).on('click', '.add-item-{{ studly_case($field['name']) }}', function () {
            const parent = $(this).parent();
            const template = parent.find('.item-template');
            const container = parent.find('.items-container');

            create_item(template, container, '#dynamicListPlaceholder');

            container.find('input, textarea').prop('disabled', false);

            container.find('.color-picker').each(function () {
                // $(this).colorpicker({});
            });
        });

        $('.items-container').find('textarea').each(function () {
            if ($(this).hasClass('summernote')) {
                $(this).summernote(summernote_options);
            }
        });

        $(document).on('click', '.remove-item', function () {
            $(this).parents('.item-group').remove();
        });
    </script>
@endpush
