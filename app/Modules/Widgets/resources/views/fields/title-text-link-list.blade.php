<div class="form-group">
    <label for="widget{{ studly_case($field['name']) }}">{{ $field['label'] }}</label>

    <div class="input-group mb-3">
        <div style="display: none;">
            <div data-item-id="#dynamicListPlaceholder" class="item-template item-group input-group mb-3 align-items-center border border-grey-light pt-2 pb-2">
                <div class="col-lg-10 mb-3">
                    <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][title]" placeholder="Заголовок" class="form-control mb-1" disabled>
                    <textarea name="{{ $field['name'] }}[#dynamicListPlaceholder][text]" placeholder="Текст" class="summernote"></textarea>
                    <select class="select2-field menu-url-select" style="width: 100%">
                        {!! app(\App\Service\MenuHelper::class)->renderSelectData(request()->get('lang')) !!}
                    </select>
                    <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][url]" style="margin-top: 20px" placeholder="Ссылка" class="form-control mb-1 disabled menu-url-input">
                </div>

                <div class="col-lg-2">
                    <button type="button" class="btn btn-danger remove-item float-right text-white">Видалити</button>
                </div>
            </div>
        </div>

        <input type="hidden" name="{{ $field['name'] }}" value="">

        <div class="items-container w-100">
            @foreach((array) old($field['name'], $value) as $key => $value)
                <div data-item-id="{{ $key }}" class="item-template item-group input-group mb-3 align-items-center border border-grey-light pt-2 pb-2">
                    <div class="col-lg-10 mb-3">
                        <input type="text" name="{{ $field['name'] }}[{{ $key }}][title]" value="{{ old($field['name'] . '.' . $key . '.title', $value['title'] ?? '') }}" placeholder="Заголовок" class="form-control mb-1">
                        <textarea name="{{ $field['name'] }}[{{ $key }}][text]" placeholder="Текст" class="summernote">{{ old($field['name'] . '.' . $key . '.text', $value['text'] ?? '') }}</textarea>
                        <select class="select2-field-shown menu-url-select" style="width: 100%">
                            <?php $sel = old($field['name'] . '.' . $key . '.url', $value['url'] ?? '') ?>
                            {!! app(\App\Service\MenuHelper::class)->renderSelectData(request()->get('lang'),$sel) !!}
                        </select>
                        <input type="text" name="{{ $field['name'] }}[{{ $key }}][url]" value="{{$sel}}" style="margin-top: 20px" placeholder="Ссылка" class="form-control mb-1 disabled menu-url-input">
                    </div>

                    <div class="col-lg-2">
                        <button type="button" class="btn btn-danger remove-item float-right text-white">Видалити</button>
                    </div>

                    @error($field['name'] . '.' . $key)
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            @endforeach
        </div>
    </div>

    <button type="button" class="btn btn-info btn-sm add-item-{{ studly_case($field['name']) }}">Додати</button>
</div>

@push('styles')
    <link rel="stylesheet" href="{{asset('matrix/libs/select2/dist/css/select2.min.css')}}">
    <style>
        input.disabled {
            pointer-events: none;
            background-color: #e9ecef;
        }
    </style>
@endpush

@push('scripts')
    <script src="{{asset('matrix/libs/select2/dist/js/select2.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
           $('.select2-field-shown').each(function () {
                $(this).select2({});

               let val = $(this).find("option:selected").val();

               if(val === '0'){
                   $(this).siblings(".menu-url-input").removeClass('disabled');
               }
            });
           
           $(document).on('change',".menu-url-select",function () {
               let val = $(this).find("option:selected").val();
               if(val !== '0'){
                   $(this).siblings(".menu-url-input").val(val);
                   $(this).siblings(".menu-url-input").addClass('disabled');
               } else {
                   $(this).siblings(".menu-url-input").val('');
                   $(this).siblings(".menu-url-input").removeClass('disabled');
               }
           })
        });

        $(document).on('click', '.add-item-{{ studly_case($field['name']) }}', function () {
            const parent = $(this).parent();
            const template = parent.find('.item-template');
            const container = parent.find('.items-container');

            create_item(template, container, '#dynamicListPlaceholder');

            container.find('input, textarea').prop('disabled', false);

            container.find('textarea').each(function () {
                if ($(this).hasClass('summernote')) {
                    $(this).summernote(summernote_options);
                }
            });

            container.find('.select2-field').each(function () {
                $(this).select2({});
            });
        });

        $('.items-container').find('textarea').each(function () {
            if ($(this).hasClass('summernote')) {
                $(this).summernote(summernote_options);
            }
        });

        $(document).on('click', '.remove-item', function () {
            $(this).parents('.item-group').remove();
        });
    </script>
@endpush
