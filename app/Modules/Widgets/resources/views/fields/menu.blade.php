<div class="form-group">
    <label for="widget{{ studly_case($field['name']) }}">{{ trans($field['label']) }}</label>

    <div class="input-group input-group-sm">
        <select name="menu_id" id="widget{{ studly_case($field['name']) }}" class="select-field {{ $field['class'] }} @error($field['name']) is-invalid @enderror" style="width: 100%">
            <option value="">---</option>
            <?php $sel = old($field['name'], $value); ?>
            @foreach(\App\Models\Menu::getTagsWithId() as $menuId => $item)
                <option value="{{$menuId}}" @if($sel == $menuId) selected @endif>{{$item}}</option>
            @endforeach
        </select>

        @error($field['name'])
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.select-field').select2();
        })
    </script>
@endpush
