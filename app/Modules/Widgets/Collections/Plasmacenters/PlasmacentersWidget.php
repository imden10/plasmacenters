<?php

namespace App\Modules\Widgets\Collections\Plasmacenters;

use App\Models\Plasmacenters;
use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class PlasmacentersWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Де можна здати кров в Biopharma Plasma? (landing)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.plasmacenters.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок (первая часть)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'title2',
                'label' => 'Заголовок (вторая часть)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'btn_text1',
                'label' => 'Текст кнопки "Записатися зараз"',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'btn_text2',
                'label' => 'Текст кнопки "Показати всі"',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ]
        ];
    }

    public function adapter($data, $lang)
    {
        $list = [];

        $plasmacenters = Plasmacenters::query()
            ->active()
            ->orderBy('sort', 'asc')
            ->get()
            ->toArray();

        if (count($plasmacenters)) {
            foreach ($plasmacenters as $key => $item) {
                $url = \App\Models\Menu::getTypesModel()[\App\Models\Menu::TYPE_PLASMACENTERS]['url_prefix'] . $item['slug'];

                if ($lang !== \App\Models\Langs::getDefaultLangCode()) {
                    $url = '/' . $lang . '/' . $url;
                } else {
                    $url = '/' . $url;
                }

                $list[$key]['url'] = $url;

                foreach ($item['translations'] as $trans) {
                    if ($trans['lang'] === $lang) {
                        $list[$key]['title']    = $trans['title'];
                        $list[$key]['city']     = $trans['city'];
                        $list[$key]['address']  = $trans['address'];
                        $list[$key]['schedule'] = $trans['schedule'];
                        $list[$key]['phone']    = $trans['phone'];
                    }
                }

                $list[$key]['coord_lat']    = $item['coord_lat'];
                $list[$key]['coord_lng']    = $item['coord_lng'];
                $list[$key]['request_link'] = $item['request_link'];
                $list[$key]['tag']          = $item['tag'];
            }
        }

        $data['list'] = $list;

        return $data;
    }
}
