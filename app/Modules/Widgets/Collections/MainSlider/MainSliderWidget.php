<?php

namespace App\Modules\Widgets\Collections\MainSlider;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class MainSliderWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Главный слайдер (landing)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.main-slider.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'main-slider-list',
                'name' => 'list',
                'label' => 'Слайдер',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }
}
