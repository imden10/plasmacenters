<?php

namespace App\Modules\Widgets\Collections\Feedback;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class FeedbackWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Форма обратной связи (landing)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.feedback.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            ['separator' => 'Левая колонка'],
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'email',
                'label' => 'E-mail',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'phone',
                'label' => 'Телефон',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'title-link-list',
                'name' => 'socials',
                'label' => 'Соц. сети',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
            ['separator' => 'Правая колонка'],
            [
                'type' => 'text',
                'name' => 'title2',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'subtitle',
                'label' => 'Подзаголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'form_name',
                'label' => 'Заголовок формы',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'btn_name',
                'label' => 'Заголовок кнопки',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Поля формы'],
            [
                'type' => 'text',
                'name' => 'input_name_placeholder',
                'label' => 'Имя (placeholder)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'input_company_placeholder',
                'label' => 'Компания (placeholder)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'input_phone_placeholder',
                'label' => 'Телефон (placeholder)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'input_email_placeholder',
                'label' => 'E-mail (placeholder)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'input_text_placeholder',
                'label' => 'Текст (placeholder)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'input_text_title',
                'label' => 'Текст (title)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
        ];
    }
}
