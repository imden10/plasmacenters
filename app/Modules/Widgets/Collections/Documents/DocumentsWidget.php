<?php

namespace App\Modules\Widgets\Collections\Documents;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class DocumentsWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Візьміть з собою документи (landing)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.documents.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'image',
                'name' => 'image',
                'label' => 'Изображение',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'description',
                'label' => 'Описание',
                'class' => '',
                'rules' => 'nullable|string|max:1000',
                'value' => '',
            ],
            [
                'type' => 'text-list',
                'name' => 'list',
                'label' => 'Список',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }
}
