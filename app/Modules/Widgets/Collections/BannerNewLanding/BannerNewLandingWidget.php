<?php

namespace App\Modules\Widgets\Collections\BannerNewLanding;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class BannerNewLandingWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Банер (new landing)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.banner-new-landing.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'image',
                'name'  => 'image',
                'label' => 'Изображение',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'image',
                'name'  => 'image_mob',
                'label' => 'Изображение (для моб)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'btn_text',
                'label' => 'Текст на кнопке',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'footer_text',
                'label' => 'Текст внизу',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Список с иконками'],
            [
                'type' => 'image-and-text-list',
                'name' => 'list_icons',
                'label' => 'Список',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
            ['separator' => 'Список обычный'],
            [
                'type' => 'text-list',
                'name' => 'list_default',
                'label' => 'Список',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }
}
