<?php

namespace App\Modules\Widgets\Collections\Droplets;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class DropletsWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Капельки (landing)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.droplets.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'image',
                'name' => 'image',
                'label' => 'Изображение',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'title_color',
                'label' => 'Цвет заголовока',
                'class' => '',
                'rules' => 'nullable|string|max:10',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'subtitle',
                'label' => 'Подзаголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'text',
                'label' => 'Текст',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'btn_text',
                'label' => 'Текст на кнопке',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ]
        ];
    }
}
