<?php

namespace App\Modules\Widgets\Collections\Steps1;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class Steps1Widget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = '4 кроки донора (landing)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.steps1.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок (первая часть)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'title2',
                'label' => 'Заголовок (вторая часть)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'step-list',
                'name' => 'list',
                'label' => 'Шаги',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }
}
