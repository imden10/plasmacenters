<?php

namespace App\Modules\Widgets\Collections\QuestionsForm;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class QuestionsFormWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Залишились питання (Форма)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.questions-form.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'subtitle',
                'label' => 'Подзаголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'image',
                'name' => 'image',
                'label' => 'Изображение',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'socials-list',
                'name'  => 'list',
                'label' => 'Ссылки',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }
}
