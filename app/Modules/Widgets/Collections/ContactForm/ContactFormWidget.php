<?php

namespace App\Modules\Widgets\Collections\ContactForm;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class ContactFormWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Контакты! (Форма)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.contact-form.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'form_name',
                'label' => 'Заголовок формы',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'btn_name',
                'label' => 'Заголовок кнопки',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Поля формы'],
            [
                'type' => 'text',
                'name' => 'input_name_placeholder',
                'label' => 'Имя (placeholder)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'input_company_placeholder',
                'label' => 'Компания (placeholder)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'input_phone_placeholder',
                'label' => 'Телефон (placeholder)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'input_email_placeholder',
                'label' => 'E-mail (placeholder)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'input_text_placeholder',
                'label' => 'Текст (placeholder)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'input_text_title',
                'label' => 'Текст (title)',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
        ];
    }
}
