<?php

namespace App\Modules\Widgets\Collections\DonorsSlider;

use App\Models\Donors;
use App\Models\Plasmacenters;
use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use Illuminate\Support\Facades\Log;

class DonorsSliderWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Донори, що рятують життя! (landing)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.donors-slider.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'btn_text',
                'label' => 'Текст кнопки',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'donors-list',
                'name'  => 'list',
                'label' => 'Доноры',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }

    public function adapter($data, $lang)
    {
        $ids  = [];
        $list = [];

        if (!empty($data['list'])) {
            foreach ($data['list'] as $item) {
                $ids[] = (int)$item['donor_id'];
            }
        }

        if (count($ids)) {
            $donors = Donors::query()
                ->whereIn('id', $ids)
                ->get()
                ->toArray();

            $i = 0;

            if (count($donors)) {
                foreach ($data['list'] as $item1) {
                    foreach ($donors as $key => $item) {
                        if($item1['donor_id'] == $item['id']){
                            $url = \App\Models\Menu::getTypesModel()[\App\Models\Menu::TYPE_DONOR]['url_prefix'] . $item['slug'];

                            if ($lang !== \App\Models\Langs::getDefaultLangCode()) {
                                $url = '/' . $lang . '/' . $url;
                            } else {
                                $url = '/' . $url;
                            }

                            $list[$i]['url'] = $url;

                            foreach ($item['translations'] as $trans) {
                                if ($trans['lang'] === $lang) {
                                    $list[$i]['title']    = $trans['title'];
                                    $list[$i]['city']     = $trans['city'];
                                }
                            }

                            $list[$i]['image']     = $item['image'];
                            $list[$i]['donations'] = $item['donations'];
                            $i++;
                        }
                    }
                }
            }
        }

        $data['list'] = $list;

        return $data;
    }
}
