<?php

namespace App\Modules\Widgets\Collections\FormNewLanding;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class FormNewLandingWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Форма (new landing)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.form-new-landing.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'image',
                'name'  => 'image',
                'label' => 'Изображение',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'phone_placeholder',
                'label' => 'Текст в поле телефона',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'btn_name',
                'label' => 'Текст на кнопке',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ]
        ];
    }
}
