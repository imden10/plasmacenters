<?php

namespace App\Modules\Widgets\Collections\BeSure;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class BeSureWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Впевнитись, що ви можете бути донором плазми (landing)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.be-sure.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Пункт 1'],
            [
                'type' => 'text',
                'name' => 'item1_s1',
                'label' => 'Строка 1',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'item1_s2',
                'label' => 'Строка 2',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'item1_caption',
                'label' => 'Текст',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Пункт 2'],
            [
                'type' => 'text',
                'name' => 'item2_s1',
                'label' => 'Строка 1',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'item2_s2',
                'label' => 'Строка 2',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'item2_caption',
                'label' => 'Текст',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Пункт 3'],
            [
                'type' => 'image',
                'name' => 'item3_image',
                'label' => 'Изображение',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'item3_caption',
                'label' => 'Текст',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
        ];
    }
}
