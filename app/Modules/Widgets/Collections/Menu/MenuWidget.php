<?php

namespace App\Modules\Widgets\Collections\Menu;

use App\Core\Error\ErrorManager;
use App\Modules\Widgets\Contracts\Widget as WidgetInterface;
use App\Service\Adapter;
use Symfony\Component\HttpFoundation\Response;

class MenuWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Меню (landing)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.menu.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'menu',
                'name' => 'menu_id',
                'label' => 'Меню',
                'class' => '',
                'rules' => 'nullable|integer',
                'value' => null
            ]
        ];
    }

    public function adapter($data, $lang)
    {
        $menu_id = $data['menu_id'];

        $main = \App\Models\Menu::query()
            ->where('id', $menu_id)
            ->where('const',  1)
            ->first();

        $model = \App\Models\Menu::query()
            ->where('tag', $main->tag)
            ->where('const', '<>', 1)
            ->with(['page','blog','landing','donor','plasmacentr'])
            ->defaultOrder()
            ->get()
            ->toTree()
            ->toArray();

        $data = app(Adapter::class)->prepareMenuResults($model,$lang);

        return $data;
    }
}
