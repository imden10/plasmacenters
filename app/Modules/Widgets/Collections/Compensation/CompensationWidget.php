<?php

namespace App\Modules\Widgets\Collections\Compensation;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class CompensationWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Компенсація (landing)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.compensation.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'subtitle',
                'label' => 'Подзаголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'description',
                'label' => 'Описание',
                'class' => '',
                'rules' => 'nullable|string|max:1000',
                'value' => '',
            ],
            [
                'type'  => 'image',
                'name'  => 'image',
                'label' => 'Изображение',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'       => 'menu-link',
                'title'      => 'Ссылка',
                'name'       => 'menu_link',
                'name_text'  => 'btn_name',
                'label_text' => 'Текст ссылки (кнопки)',
                'name_url'   => 'btn_link',
                'label_url'  => 'Ссылка',
                'value'      => [],
                'rules'      => 'nullable|array',
            ],
        ];
    }
}
