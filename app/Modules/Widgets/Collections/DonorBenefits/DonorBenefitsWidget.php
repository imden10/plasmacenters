<?php

namespace App\Modules\Widgets\Collections\DonorBenefits;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class DonorBenefitsWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Плюшки та вигоди донора (landing)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.donor-benefits.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'image',
                'name' => 'image',
                'label' => 'Изображение',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'title-text-link-list',
                'name' => 'list',
                'label' => 'Список',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }
}
