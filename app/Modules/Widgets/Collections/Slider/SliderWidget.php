<?php

namespace App\Modules\Widgets\Collections\Slider;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class SliderWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Слайдер (landing)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.slider.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'image-list',
                'name' => 'list',
                'label' => 'Изображения',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }
}
