<?php

return [

    /*
    |------------------------------------------------------------------
    | View layout component
    |------------------------------------------------------------------
    */
    //    'view-layout' => 'admin::layout',
    'view-layout'  => 'admin::layout',

    /*
    |------------------------------------------------------------------
    | View layout title
    |------------------------------------------------------------------
    */
    'layout-title' => 'widgets::strings.layout.title',


    'widgets'           => [
        'main-slider'            => \App\Modules\Widgets\Collections\MainSlider\MainSliderWidget::class,
        'steps1'                 => \App\Modules\Widgets\Collections\Steps1\Steps1Widget::class,
        'steps2'                 => \App\Modules\Widgets\Collections\Steps2\Steps2Widget::class,
        'full-image'             => \App\Modules\Widgets\Collections\FullImage\FullImageWidget::class,
        'donor-benefits'         => \App\Modules\Widgets\Collections\DonorBenefits\DonorBenefitsWidget::class,
        'how-everything-goes'    => \App\Modules\Widgets\Collections\HowEverythingGoes\HowEverythingGoesWidget::class,
        'join-us'                => \App\Modules\Widgets\Collections\JoinUs\JoinUsWidget::class,
        'blog-join-us'           => \App\Modules\Widgets\Collections\BlogJoinUs\BlogJoinUsWidget::class,
        'join-us-form'           => \App\Modules\Widgets\Collections\JoinUsForm\JoinUsFormWidget::class,
        'plasmacenters'          => \App\Modules\Widgets\Collections\Plasmacenters\PlasmacentersWidget::class,
        'donors-slider'          => \App\Modules\Widgets\Collections\DonorsSlider\DonorsSliderWidget::class,
        'droplets'               => \App\Modules\Widgets\Collections\Droplets\DropletsWidget::class,
        'menu'                   => \App\Modules\Widgets\Collections\Menu\MenuWidget::class,
        'share'                  => \App\Modules\Widgets\Collections\Share\ShareWidget::class,
        'contact-form'           => \App\Modules\Widgets\Collections\ContactForm\ContactFormWidget::class,
        'questions-form'         => \App\Modules\Widgets\Collections\QuestionsForm\QuestionsFormWidget::class,
        'simple-text'            => \App\Modules\Widgets\Collections\SimpleText\SimpleTextWidget::class,
        'slider'                 => \App\Modules\Widgets\Collections\Slider\SliderWidget::class,
        'history'                => \App\Modules\Widgets\Collections\History\HistoryWidget::class,
        'certificates'           => \App\Modules\Widgets\Collections\Certificates\CertificatesWidget::class,
        'feedback'               => \App\Modules\Widgets\Collections\Feedback\FeedbackWidget::class,
        'be-sure'                => \App\Modules\Widgets\Collections\BeSure\BeSureWidget::class,
        'documents'              => \App\Modules\Widgets\Collections\Documents\DocumentsWidget::class,
        'remuneration'           => \App\Modules\Widgets\Collections\Remuneration\RemunerationWidget::class,
        'compensation'           => \App\Modules\Widgets\Collections\Compensation\CompensationWidget::class,
        'recover-after-donation' => \App\Modules\Widgets\Collections\RecoveryAfterDonation\RecoveryAfterDonationWidget::class,
        'whats-next'             => \App\Modules\Widgets\Collections\WhatsNext\WhatsNextWidget::class,
        'sign-next-donation'     => \App\Modules\Widgets\Collections\SignNextDonation\SignNextDonationWidget::class,
        'blog-subscribe'         => \App\Modules\Widgets\Collections\BlogSubscribe\BlogSubscribeWidget::class,
        'banner-new-landing'     => \App\Modules\Widgets\Collections\BannerNewLanding\BannerNewLandingWidget::class,
        'rewards-new-landing'    => \App\Modules\Widgets\Collections\RewardsNewLanding\RewardsNewLandingWidget::class,
        'bonus-plus-new-landing' => \App\Modules\Widgets\Collections\BonusPlusNewLanding\BonusPlusNewLandingWidget::class,
        'fund-new-landing'       => \App\Modules\Widgets\Collections\FundNewLanding\FundNewLandingWidget::class,
        'footer-new-landing'     => \App\Modules\Widgets\Collections\FooterNewLanding\FooterNewLandingWidget::class,
        'form-new-landing'       => \App\Modules\Widgets\Collections\FormNewLanding\FormNewLandingWidget::class,
    ],

    /*
    |------------------------------------------------------------------
    | Permissions for manipulation widgets
    |------------------------------------------------------------------
    */
    'permissions'       => [
        'widgets.view'   => function ($user) {
            return true;
            //return $user->isSuperUser() || $user->hasPermission('update_setting');
        },
        'widgets.create' => function ($user) {
            return true;
            //return $user->isSuperUser() || $user->hasPermission('update_setting');
        },
        'widgets.update' => function ($user) {
            return true;
            //return $user->isSuperUser() || $user->hasPermission('update_setting');
        },
        'widgets.delete' => function ($user) {
            return true;
            //return $user->isSuperUser() || $user->hasPermission('update_setting');
        },
    ],

    /*
    |------------------------------------------------------------------
    | Middleware for settings
    |------------------------------------------------------------------
    */
    'middleware'        => ['web', 'auth', 'verified'],

    /*
    |------------------------------------------------------------------
    | Uri Route prefix
    |------------------------------------------------------------------
    */
    'uri_prefix'        => 'admin',

    /*
    |------------------------------------------------------------------
    | Route name prefix
    |------------------------------------------------------------------
    */
    'route_name_prefix' => 'admin.',

    /*
    |------------------------------------------------------------------
    | Request lang key
    |------------------------------------------------------------------
    */
    'request_lang_key'  => 'lang',

];
