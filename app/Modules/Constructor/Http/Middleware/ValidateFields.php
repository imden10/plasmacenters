<?php

namespace App\Modules\Constructor\Http\Middleware;

use Closure;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ValidateFields
{
    /**
     * @var Factory
     */
    protected Factory $validator;

    /**
     * @var Repository
     */
    protected Repository $config;

    /**
     * ValidateFields constructor.
     *
     * @param Factory $validator
     * @param Repository $config
     */
    public function __construct(Factory $validator, Repository $config)
    {
        $this->validator = $validator;

        $this->config = $config;
    }

    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws BindingResolutionException
     * @throws ValidationException
     */
    public function handle(Request $request, Closure $next)
    {
        if (
            ($request->isMethod('post') || $request->isMethod('put'))
            && ($request->has($this->config->get('constructor.fields_name')) && $request->has('entity_name'))
        ) {
            $entity = app()->make($request->input('entity_name'));
            $entity->entityConstructorId($request->input('entity_id'));

            $this->validator->make(
                $request->input(),
                $entity->rulesConstructorComponentFields($request->input($this->config->get('constructor.fields_name')))
            )->validate();
        }

        return $next($request);
    }
}
