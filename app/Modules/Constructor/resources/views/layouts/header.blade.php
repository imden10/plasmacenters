<div id="component-{{ $key }}-{{$lang}}" class="card card-default {{ $name }} {{$name}}_{{$lang}} card-component" data-component-id="{{ $key }}">
    <div class="display-layout"></div>

    <div class="confirm-delete-component-popup" style="display: none;">
        <h5 class="text-sm">Удалить?</h5>
        <button class="btn btn-sm btn-secondary confirm-button" type="button" data-action="cancel">Отмена</button>
        <button class="btn btn-sm btn-danger confirm-button text-white" type="button" data-action="confirm">Удалить</button>
    </div>

    <div class="card card-outline card-info pt-1 pr-3 pb-1 pl-3 mb-0">
        <div class="card-title move-label d-inline">
            <i class="fas fa-braille mr-3"></i>
            <?php
            if($params['shown_name'] != 'widget'){
                $shownName = isset($content[$params['shown_name']]) ? mb_strimwidth($content[$params['shown_name']], 0, 30, "...") : '';
                $resTitle = '<span style="font-weight: normal">'.$shownName.'</span>';
            } else {
                if(isset($content['widget'])){
                    $model = \App\Modules\Widgets\Models\Widget::query()->where('id',$content['widget'])->first();
                    try {
                        $shownName = mb_strimwidth($model->name, 0, 30, "...");
                        $linkEdit = '/admin/widgets/'.$model->id.'/edit?lang=' . $lang;
                        $resTitle ='<a target="_blank" href="'.$linkEdit.'" title="Редагувати" style="font-weight: normal">'.$shownName.'</a>';
                    } catch(Exception $e){
                        $resTitle = '';
                    }
                } else {
                    $resTitle = '';
                }
            }
            ?>
            {{ trans($label) }} - {!! $resTitle !!}

            <span class="float-right">
                <div class="d-inline component-visibility-switch custom-switch custom-switch-off-danger custom-switch-on-success">
                    <input type="hidden" name="{{ constructor_field_name($key, 'visibility') }}" value="0">
                    <input type="checkbox" name="{{ constructor_field_name($key, 'visibility') }}" class="custom-control-input show-hide-checkbox" id="componentVisibility{{ $key }}" value="1" @if ($visibility == 1) checked @endif>
                    <label class="custom-control-label" for="componentVisibility{{ $key }}"></label>
                </div>

                <a href="#" class="link-inherit text-danger ml-2 remove-component" title="{{ trans('constructor::strings.buttons.delete') }}">
                    <i class="fas fa-trash"></i>
                </a>

                 <a href="#collapse{{ $key }}_{{$lang}}" class="text-info collapse-button ml-2" data-toggle="collapse" aria-expanded="true">
                    <i class="far fa-caret-square-down"></i>
                </a>
            </span>
        </div>
    </div>

    <input type="hidden" name="{{ constructor_field_name($key, 'position') }}" value="{{ $position }}" class="position-component">
    <input type="hidden" name="{{ constructor_field_name($key, 'component') }}" value="{{ $name }}">
