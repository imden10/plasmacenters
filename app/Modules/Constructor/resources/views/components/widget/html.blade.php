@include('constructor::layouts.header',['lang' => $lang])

<div id="collapse{{ $key }}_{{$lang}}" class="card-body mt-1 collapse show">
    <div class="row">
        <div class="col-12 mb-3 input-group-sm">
            <select name="{{ constructor_field_name($key, 'content.widget') }}" class="form-control @error(constructor_field_name_dot($key, 'content.widget')) is-invalid @enderror">
                @foreach($params['widgets']($lang) as $id => $name)
                    <option value="{{ $id }}" @if (isset($content['widget']) && $content['widget'] == $id) selected @endif>{{ $name }}</option>
                @endforeach
            </select>

            @error(constructor_field_name_dot($key, 'content.widget'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>

@include('constructor::layouts.footer')
