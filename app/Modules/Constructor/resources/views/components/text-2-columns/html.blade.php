@include('constructor::layouts.header',['lang' => $lang])

<div id="collapse{{ $key }}_{{$lang}}" class="card-body mt-1 collapse show">
    <div class="row">
        <div class="col-12 mb-3 input-group-sm">
            <input type="text" placeholder="{{ trans($params['labels']['title']) }}" class="form-control @error(constructor_field_name_dot($key, 'content.title')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.title') }}" value="{{ old(constructor_field_name_dot($key, 'content.title'), $content['title'] ?? '') }}">

            @error(constructor_field_name_dot($key, 'content.title'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6">
            <textarea class="form-control summernote @error(constructor_field_name_dot($key, 'content.text1')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.text1') }}">{{ old(constructor_field_name_dot($key, 'content.text1'), $content['text1'] ?? '') }}</textarea>

            @error(constructor_field_name_dot($key, 'content.text1'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6">
            <textarea class="form-control summernote @error(constructor_field_name_dot($key, 'content.text2')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.text2') }}">{{ old(constructor_field_name_dot($key, 'content.text2'), $content['text2'] ?? '') }}</textarea>

            @error(constructor_field_name_dot($key, 'content.text2'))
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>

@include('constructor::layouts.footer')
