@include('constructor::layouts.header',['lang' => $lang])

<?php
$donors = \App\Models\Donors::query()->active()->get();
?>

<div id="collapse{{ $key }}_{{$lang}}" class="card-body mt-1 collapse show show">
    <div class="row">
        <div class="input-group">
            <div style="display: none;">
                <div data-item-id="#imageInputPlaceholder1" class="donors-list-template item-group m-1 border border-grey-light p-1 d-flex align-items-center">
                    <div class="col-10">
                        <select name="{{ constructor_field_name($key, 'content.list') }}[#imageInputPlaceholder1][donor_id]" class="select2-field2">
                            <option value="">{{ $params['labels']['title'] }}</option>
                            @foreach($donors as $donor)
                                <option value="{{$donor->id}}">{{$donor->title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-2">
                        <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                    </div>
                </div>
            </div>

            <input type="hidden" name="{{ constructor_field_name($key, 'content.list') }}" value="">

            <div class="donors-list-container w-100">
                @foreach((array) old(constructor_field_name($key, 'content.list'), $content['list'] ?? []) as $k => $value)
                    <div data-item-id="{{ $k }}" class="item-template item-group m-1 border border-grey-light p-1 d-flex align-items-center">
                        <div class="col-10">
                            <select name="{{ constructor_field_name($key, 'content.list') }}[{{ $k }}][donor_id]" class="select2-field2-shown">
                                @foreach($donors as $donor)
                                    <option value="{{$donor->id}}" @if(($value['donor_id'] ?? '') == $donor->id) selected @endif>{{$donor->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-2">
                            <button type="button" class="btn btn-danger remove-item text-white float-right">Удалить</button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <button type="button" class="btn btn-info btn-sm add-donors-list-item_{{$lang}} d-block mt-2">Добавить</button>
    </div>
</div>

@include('constructor::layouts.footer')
