<script type="text/javascript">
    (function () {
        $('.select2-field2-shown').each(function () {
            $(this).select2({});
        });

        $(document).on('click', '.add-donors-list-item_{{$lang}}', function () {
            const template = $(this).parent().find('.donors-list-template');
            const container = $(this).parent().find('.donors-list-container');

            create_item(template, container, '#imageInputPlaceholder1');

            container.find('input, textarea').prop('disabled', false);

            container.find('.select2-field2').each(function () {
                $(this).select2({});
            });
        });

        $(document).on('click', '.remove-item', function () {
            $(this).parents('.item-group').remove();
        });
    })();
</script>
