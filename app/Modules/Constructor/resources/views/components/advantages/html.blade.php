@include('constructor::layouts.header',['lang' => $lang])

<div id="collapse{{ $key }}_{{$lang}}" class="card-body mt-1 collapse show">
    <div class="row">
        <div class="input-group">
            <div style="display: none;">
                <div data-item-id="#imageInputPlaceholder1" class="advantages-list-template item-group m-1 border border-grey-light p-1 d-flex align-items-center">
                    <div class="col-2">
                        <input type="text" name="{{ constructor_field_name($key, 'content.list') }}[#imageInputPlaceholder1][title]" placeholder="{{ $params['labels']['title'] }}" class="form-control mt-3 mb-1" disabled>
                    </div>

                    <div class="col-6">
                        <input type="text" name="{{ constructor_field_name($key, 'content.list') }}[#imageInputPlaceholder1][text]" placeholder="{{ $params['labels']['text'] }}" class="form-control mt-3 mb-1" disabled>
                    </div>

                    <div class="col-2">
                        <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                    </div>
                </div>
            </div>

            <input type="hidden" name="{{ constructor_field_name($key, 'content.list') }}" value="">

            <div class="advantages-list-container w-100">
                @foreach((array) old(constructor_field_name($key, 'content.list'), $content['list'] ?? []) as $k => $value)
                    <div data-item-id="{{ $k }}" class="item-template item-group m-1 border border-grey-light p-1 d-flex align-items-center">
                        <div class="col-2">
                            <input type="text" name="{{ constructor_field_name($key, 'content.list') }}[{{ $k }}][title]" placeholder="{{ $params['labels']['title'] }}" class="form-control mt-3 mb-1" value="{{ $value['title'] ?? '' }}">
                        </div>

                        <div class="col-2">
                            <input type="text" name="{{ constructor_field_name($key, 'content.list') }}[{{ $k }}][text]" placeholder="{{ $params['labels']['text'] }}" class="form-control mt-3 mb-1" value="{{ $value['text'] ?? '' }}">
                        </div>

                        <div class="col-2">
                            <button type="button" class="btn btn-danger remove-item text-white float-right">Удалить</button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <button type="button" class="btn btn-info btn-sm add-advantages-list-item_{{$lang}} d-block mt-2">Добавить</button>
    </div>
</div>

@include('constructor::layouts.footer')
