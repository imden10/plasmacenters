<script type="text/javascript">
    (function () {
        $('.select2-field2-shown').each(function () {
            $(this).select2({});
        });

        $(document).on('click', '.add-question-list-item_{{$lang}}', function () {
            const template = $(this).parent().find('.question-list-template');
            const container = $(this).parent().find('.question-list-container');

            create_item(template, container, '#imageInputPlaceholder1');

            container.find('input, textarea').prop('disabled', false);

            container.find('textarea').each(function () {
                console.log('this - ',$(this))
                if ($(this).hasClass('summernote2')) {
                    console.log('summernote shown 1');
                    $(this).summernote(summernote_options);
                }
            });

            container.find('.select2-field2').each(function () {
                $(this).select2({});
            });
        });

        $(document).on('click', '.remove-item', function () {
            $(this).parents('.item-group').remove();
        });
    })();
</script>
