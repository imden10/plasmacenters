const MEDIA_PATH_PREFIX = '/storage/app/public/media';
const FILE_PATH_PREFIX = '/storage/app/public/files';

export { MEDIA_PATH_PREFIX, FILE_PATH_PREFIX }