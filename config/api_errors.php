<?php

/** Errors list */

define("VALIDATION_REQUEST_JSON_EXPECTED", 101);
define("VALIDATION_REQUIRED_FIELD", 102);
define("VALIDATION_MODEL_NOT_FOUND", 103);
define("VALIDATION_MENU_NOT_FOUND", 104);
define("VALIDATION_PLASMACENTER_NOT_FOUND", 105);
define("VALIDATION_DATE_FORMAT_NOT_VALID", 106);
define("VALIDATION_TIME_FORMAT_NOT_VALID", 107);
define("VALIDATION_EMAIL_NOT_VALID", 108);
define("VALIDATION_EXCEPTION", 109);

return [
    VALIDATION_REQUEST_JSON_EXPECTED  => [
        'message' => 'The requested API method is waiting for json input',
    ],
    VALIDATION_REQUIRED_FIELD         => [
        'message' => 'Required field',
    ],
    VALIDATION_MODEL_NOT_FOUND        => [
        'message' => 'Model not found',
    ],
    VALIDATION_MENU_NOT_FOUND         => [
        'message' => 'Menu not found',
    ],
    VALIDATION_PLASMACENTER_NOT_FOUND => [
        'message' => 'Plasmacenter not found',
    ],
    VALIDATION_DATE_FORMAT_NOT_VALID  => [
        'message' => 'Date format not valid, we need in the format \'YYYY-MM-DD\'',
    ],
    VALIDATION_TIME_FORMAT_NOT_VALID  => [
        'message' => 'Time format not valid, we need in the format \'HH:MM\'',
    ],
    VALIDATION_EMAIL_NOT_VALID        => [
        'message' => 'Email not valid',
    ],
    VALIDATION_EXCEPTION        => [
        'message' => 'Exception',
    ]
];
