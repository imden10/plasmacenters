<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlasmacenterTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plasmacenter_translations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('plasmacenters_id');
            $table->string('lang',10);
            $table->string('title',255)->nullable();
            $table->string('city',255)->nullable();
            $table->string('address',255)->nullable();
            $table->string('schedule',255)->nullable();
            $table->string('email',255)->nullable();
            $table->string('phone',255)->nullable();
            $table->text('description')->nullable();
            $table->string('meta_title',255)->nullable();
            $table->string('meta_keywords',255)->nullable();
            $table->string('meta_description',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plasmacenter_translations');
    }
}
