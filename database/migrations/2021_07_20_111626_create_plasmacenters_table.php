<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlasmacentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plasmacenters', function (Blueprint $table) {
            $table->id();
            $table->string('slug',255)->unique();
            $table->tinyInteger('status');
            $table->string('type',255)->default('plasmacenter');
            $table->string('coord_lat',20)->nullable();
            $table->string('coord_lng',20)->nullable();
            $table->text('socials')->default(null);
            $table->integer('menu_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plasmacenters');
    }
}
