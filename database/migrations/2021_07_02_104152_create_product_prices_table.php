<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_prices', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id');
            $table->integer('count_position');
            $table->integer('count_position_unit_id');
            $table->integer('retail_count');
            $table->integer('retail_count_unit_id');
            $table->decimal('retail_price',10,2);
            $table->integer('wholesale_count');
            $table->integer('wholesale_count_unit_id');
            $table->decimal('wholesale_price',10,2);
            $table->boolean('status')->default(true);
            $table->integer('order')->default(0);
            $table->boolean('is_main')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_prices');
    }
}
