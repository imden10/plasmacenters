@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item"><a href="{{route('users-admin.index')}}">Адміністратори</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редагувати</li>
        </ol>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                @include('admin.users-admin._form',['model' => $model, 'action' => 'edit'])
            </div>
        </div>
    </div>
</div>
@endsection
