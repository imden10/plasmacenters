@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item active" aria-current="page">Адміністратори</li>
            <a href="{{route('users-admin.create')}}" class="btn btn-primary">
                <i class="fa fa-plus"></i>
                Додати
            </a>
        </ol>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Ім'я</th>
                            <th>Email</th>
                            <th>Телефон</th>
                            <th>Дії</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr data-id="{{$user->id}}">
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->phone}}</td>
                                <td>
                                    <div style="display: flex">
                                        <div style="margin-left: 10px">
                                            <form action="{{ route('users-admin.destroy', $user->id) }}" method="POST">

                                                <a href="{{ route('users-admin.edit', $user->id) }}" class="btn btn-primary text-white" title="Редагувати">
                                                    <i class="fas fa-edit  fa-lg"></i>
                                                </a>
                                                &nbsp;
                                                @csrf
                                                @method('DELETE')

                                                @if($user->id != \Illuminate\Support\Facades\Auth::user()->id)
                                                <a href="javascript:void(0)" title="Видалити" class="btn btn-danger delete-item-btn text-white">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                                @endif
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $users->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <style>
        nav.breadcrumb-nav {
            position: relative;
        }
        nav.breadcrumb-nav a.btn {
            position: absolute;
            right: 15px;
            top: 4px;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $(document).ready(() => {
            $('.delete-item-btn').on('click',function() {
                if(confirm('You definitely want to delete the user?')){
                    $(this).closest('form').submit();
                }
            });
        });
    </script>
@endpush
