<h4>Добавьте меню</h4>

<select class="select2-field menu-select" style="width: 100%">
    <?= app(\App\Service\MenuHelper::class)->renderSelectData() ?>
</select>

<div class="menu-container connectedSortable" id="sortable">
@if(count($menu))
    @foreach($menu as $menuKey => $item)
            @include('admin.pages._menu_elem',[
                'title'      => $item->model_title,
                'modelId'    => $item->model_id,
                'modelClass' => $item->model_type,
                'modelTitle' => $item->model_title,
                'modelSlug'  => $item->model_slug,
                'typeName'   => \App\Service\MenuHelper::namesFromClass[$item->model_type],
                'randKey'    => $menuKey
        ])
    @endforeach
@endif
</div>

@push('styles')
    <style>
        .menu-container {
            margin: 30px 0;
            padding: 3px 6px;
            border: 1px #cccccc4d solid;
            min-height: 40px;
        }

        .menu-container .menu-elem {
            border: 1px #ccc solid;
            padding: 5px 10px;
            display: flex;
            justify-content: space-between;
            align-items: center;
            cursor: move;
            margin: 3px 0;
        }

        .menu-container .menu-elem .r .placeholder-type {
            margin-right: 15px;
            color: #ccc;
        }

        .menu-container .menu-elem .r .remove-btn {
            width: 30px;
            display: inline-flex;
            height: 30px;
            align-items: center;
            justify-content: center;
            margin: -5px -10px -5px -10px;
            cursor: pointer;
            transition: all .3s;
        }

        .menu-container .menu-elem .r .remove-btn:hover {
            transform: scale(1.1);
            transition: all .3s;
        }
    </style>
@endpush
