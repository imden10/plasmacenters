@extends('layouts.admin.app')

@section('content')
<nav aria-label="breadcrumb" class="breadcrumb-nav">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
        <li class="breadcrumb-item active" aria-current="page">Меню</li>
    </ol>
</nav>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body" id="admin_menu">
                <div class="form-row">
                    <div class="col-md-6">
                        <form action="{{route('menu.index')}}" method="get">
                            <div class="row">
                                <div class="form-group col-md-4">Выберите меню для изменения:</div>
                                <div class="form-group col-md-4">
                                    <select name="tag" class="form-control">
                                        @foreach(\App\Models\Menu::getTags() as $key => $item)
                                            <option value="{{$key}}" @if($key === $tag) selected @endif>{{$item}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <input type="submit" value="Вибрати" class="btn btn-primary text-white">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form action="{{route('menu.add-menu')}}" method="post">
                            @csrf
                            <div class="row float-right">
                                <div class="form-group col-4">Создать новое</div>
                                <div class="form-group col-5">
                                    <input type="text" name="tag" placeholder="Название меню" class="form-control">
                                    <input type="hidden" name="const" value="1">
                                </div>
                                <div class="form-group col-3">
                                    <input type="submit" value="Добавить" class="btn btn-success text-white">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            @if($tag)
                    <div class="row">
                        <div class="col-3">
                            <div class="card">
                                <div class="card-header">Добавить в меню</div>
                                <div class="card-body">
                                        <div id="accordion">

                                            <div class="card">
                                                <div class="card-header collapsed" style="cursor: pointer;" id="headingPage" data-toggle="collapse" data-target="#collapsePage" aria-expanded="false" aria-controls="collapsePage">
                                                    Страницы
                                                </div>
                                                <div id="collapsePage" class="collapse" aria-labelledby="headingPage" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <form action="{{route('menu.add-item')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="tag" value="{{$tag}}">
                                                            <input type="hidden" name="type" value="{{\App\Models\Menu::TYPE_PAGE}}">
                                                            <input type="hidden" name="tree" value="{{$model}}">

                                                            <div class="form-group col-12">
                                                                <input type="text" name="name" placeholder="Название" class="form-control">
                                                            </div>
                                                            <div class="form-group col-12">
                                                                <select name="model_id" id="url_page" class="select2-elem" style="width: 100%">
                                                                    @foreach(\App\Models\Pages::query()->get() as $item)
                                                                        <option value="{{$item->id}}" title="{{$item->title}}">{{mb_strimwidth($item->title, 0, 30, "...")}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-12">
                                                                <input type="submit" value="Добавить" style="margin-bottom: 15px;" class="btn btn-success text-white float-right">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card">
                                                <div class="card-header collapsed" style="cursor: pointer;" id="headingBlog" data-toggle="collapse" data-target="#collapseBlog" aria-expanded="false" aria-controls="collapseBlog">
                                                    Блог
                                                </div>
                                                <div id="collapseBlog" class="collapse" aria-labelledby="headingBlog" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <form action="{{route('menu.add-item')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="tag" value="{{$tag}}">
                                                            <input type="hidden" name="type" value="{{\App\Models\Menu::TYPE_BLOG}}">
                                                            <input type="hidden" name="tree" value="{{$model}}">

                                                            <div class="form-group col-12">
                                                                <input type="text" name="name" placeholder="Название" class="form-control">
                                                            </div>
                                                            <div class="form-group col-12">
                                                                <select name="model_id" id="url_blog" class="select2-elem" style="width: 100%">
                                                                    @foreach(\App\Models\BlogArticles::query()->get() as $item)
                                                                        <option value="{{$item->id}}" title="{{$item->name}}">{{mb_strimwidth($item->name, 0, 30, "...")}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-12">
                                                                <input type="submit" value="Добавить" style="margin-bottom: 15px;" class="btn btn-success text-white float-right">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card">
                                                <div class="card-header collapsed" style="cursor: pointer;" id="headingLanding" data-toggle="collapse" data-target="#collapseLanding" aria-expanded="false" aria-controls="collapseLanding">
                                                    Лендинг
                                                </div>
                                                <div id="collapseLanding" class="collapse" aria-labelledby="headingLanding" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <form action="{{route('menu.add-item')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="tag" value="{{$tag}}">
                                                            <input type="hidden" name="type" value="{{\App\Models\Menu::TYPE_LANDING}}">
                                                            <input type="hidden" name="tree" value="{{$model}}">

                                                            <div class="form-group col-12">
                                                                <input type="text" name="name" placeholder="Название" class="form-control">
                                                            </div>
                                                            <div class="form-group col-12">
                                                                <select name="model_id" id="url_blog" class="select2-elem" style="width: 100%">
                                                                    @foreach(\App\Models\Landing::query()->get() as $item)
                                                                        <option value="{{$item->id}}" title="{{$item->title}}">{{mb_strimwidth($item->title, 0, 30, "...")}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-12">
                                                                <input type="submit" value="Добавить" style="margin-bottom: 15px;" class="btn btn-success text-white float-right">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card">
                                                <div class="card-header collapsed" style="cursor: pointer;" id="headingDonors" data-toggle="collapse" data-target="#collapseDonors" aria-expanded="false" aria-controls="collapseDonors">
                                                    Доноры
                                                </div>
                                                <div id="collapseDonors" class="collapse" aria-labelledby="headingDonors" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <form action="{{route('menu.add-item')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="tag" value="{{$tag}}">
                                                            <input type="hidden" name="type" value="{{\App\Models\Menu::TYPE_DONOR}}">
                                                            <input type="hidden" name="tree" value="{{$model}}">

                                                            <div class="form-group col-12">
                                                                <input type="text" name="name" placeholder="Название" class="form-control">
                                                            </div>
                                                            <div class="form-group col-12">
                                                                <select name="model_id" id="url_blog" class="select2-elem" style="width: 100%">
                                                                    @foreach(\App\Models\Donors::query()->get() as $item)
                                                                        <option value="{{$item->id}}" title="{{$item->title}}">{{mb_strimwidth($item->title, 0, 30, "...")}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-12">
                                                                <input type="submit" value="Добавить" style="margin-bottom: 15px;" class="btn btn-success text-white float-right">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card">
                                                <div class="card-header collapsed" style="cursor: pointer;" id="headingPlasmacenters" data-toggle="collapse" data-target="#collapsePlasmacenters" aria-expanded="false" aria-controls="collapsePlasmacenters">
                                                    Плазмацентры
                                                </div>
                                                <div id="collapsePlasmacenters" class="collapse" aria-labelledby="headingPlasmacenters" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <form action="{{route('menu.add-item')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="tag" value="{{$tag}}">
                                                            <input type="hidden" name="type" value="{{\App\Models\Menu::TYPE_PLASMACENTERS}}">
                                                            <input type="hidden" name="tree" value="{{$model}}">

                                                            <div class="form-group col-12">
                                                                <input type="text" name="name" placeholder="Название" class="form-control">
                                                            </div>
                                                            <div class="form-group col-12">
                                                                <select name="model_id" id="url_blog" class="select2-elem" style="width: 100%">
                                                                    @foreach(\App\Models\Plasmacenters::query()->get() as $item)
                                                                        <option value="{{$item->id}}" title="{{$item->title}}">{{mb_strimwidth($item->title, 0, 30, "...")}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-12">
                                                                <input type="submit" value="Добавить" style="margin-bottom: 15px;" class="btn btn-success text-white float-right">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card">
                                                <div class="card-header collapsed" style="cursor: pointer;" id="headingLink" data-toggle="collapse" data-target="#collapseLink" aria-expanded="false" aria-controls="collapseLink">
                                                    Произвольные ссылки
                                                </div>
                                                <div id="collapseLink" class="collapse" aria-labelledby="headingLink" data-parent="#accordion">
                                                    <div class="card-body">
                                                        <form action="{{route('menu.add-item')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="tag" value="{{$tag}}">
                                                            <input type="hidden" name="type" value="{{\App\Models\Menu::TYPE_ARBITRARY}}">
                                                            <input type="hidden" name="tree" value="{{$model}}">

                                                            <div class="form-group col-12">
                                                                <input type="text" name="name" placeholder="Название" class="form-control">
                                                            </div>
                                                            <div class="form-group col-12">
                                                                <input type="text" name="url" placeholder="URL" class="form-control">
                                                            </div>
                                                            <div class="form-group col-12">
                                                                <input type="submit" value="Добавить" style="margin-bottom: 15px;" class="btn btn-success text-white float-right">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-9">
                            <div class="card">
                                <div class="card-header">Выбраное меню &laquo;{{$tag}}&raquo;</div>
                                <div class="card-body">
                                    <TreeCategories :categories="{{$model}}" :types="{{json_encode(\App\Models\Menu::getTypes())}}"></TreeCategories>

                                    <form action="{{route('menu.delete-menu')}}" method="post" id="delete_menu_form">
                                        @csrf
                                        <input type="hidden" name="tag" value="{{$tag}}">
                                    </form>
                                    <span class="btn btn-danger text-white float-right delete-menu-btn">Удалить меню</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <h3>Выберите меню</h3>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{asset('css/admin/menu.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/select2/css/select2.min.css')}}">
    <style>
        nav.breadcrumb-nav {
            position: relative;
        }
        nav.breadcrumb-nav a.btn {
            position: absolute;
            right: 15px;
            top: 4px;
        }
    </style>
@endpush

@push('scripts')
    <script src="{{asset('/js/admin_menu.js')}}"></script>
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('.select2-elem').each(function () {
                $(this).select2({
                    placeholder: "Оберіть із списку",
                    allowClear: true,
                }).on('change', function (e) {
                    $(this).closest('form').find('input[name="name"]').val($(this).find(':selected').text());
                });
            });

            $('.select2-elem').each(function () {
                $(this).val('-1').trigger('change');
            });

            $('.delete-menu-btn').on('click',function () {
                Swal.fire({
                    title: 'Вы уверенны?',
                    text: "Вы пытаетесь удалить меню!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Да, сделать это!',
                    cancelButtonText: 'Нет'
                }).then((result) => {
                    if (result.value) {
                        $("#delete_menu_form").submit();
                    }
                })
            })
        });
    </script>
@endpush
