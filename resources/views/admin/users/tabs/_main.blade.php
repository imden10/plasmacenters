<table class="table table-striped">
    <tbody>
    <tr class="tr-data-list">
        <td style="width: 30%"><strong>ПІБ</strong></td>
        <td>{{$model->name}}</td>
    </tr>
    <tr class="tr-data-list">
        <td style="width: 30%"><strong>Email</strong></td>
        <td>{{$model->email}}</td>
    </tr>
    <tr class="tr-data-list">
        <td style="width: 30%"><strong>Телефон</strong></td>
        <td>{{$model->phone}}</td>
    </tr>
    <tr class="tr-data-list">
        <td style="width: 30%"><strong>Статус</strong></td>
        <td>
            <span class="badge"
                  style="color: {{\App\Models\User::getStatusColors()[$model->status][1]}}; background-color:{{\App\Models\User::getStatusColors()[$model->status][0]}}"
            >{{\App\Models\User::getStatuses()[$model->status]}}</span>
        </td>
    </tr>
    <tr class="tr-data-list">
        <td style="width: 30%"><strong>Зареєстрований</strong></td>
        <td>{{$model->created_at->format('d.m.Y')}}</td>
    </tr>
    </tbody>
</table>
