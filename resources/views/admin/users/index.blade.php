@extends('layouts.admin.app')

@section('content')
    @include('admin.users._modal_change_status')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item active" aria-current="page">Користувачі</li>
        </ol>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="form-row">

                        <div class="form-group col-md-3">
                            <label>Статус</label>
                            <select name="status" class="select2 form-control m-t-15">
                                <option value="">---</option>
                                @foreach(\App\Models\User::getStatuses() as $key => $status)
                                    <option value="{{$key}}" @if(old('status', request()->input('status')) == (string)$key) selected @endif>{{$status}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="inputPassword4">&nbsp;</label>
                            <button type="submit" class="btn btn-success form-control text-white">Фільтрувати</button>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputPassword4">&nbsp;</label>
                            <a href="{{ route('users.index') }}" class="btn btn-danger form-control text-white">Скинути</a>
                        </div>
                    </div>
                </form>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Ім’я</th>
                            <th>Email</th>
                            <th>Телефон</th>
                            <th>Статус</th>
                            <th>Дії</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr data-id="{{$user->id}}">
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->phone}}</td>
                                <td>
                                    <span class="badge"
                                          style="color: {{\App\Models\User::getStatusColors()[$user->status][1]}}; background-color:{{\App\Models\User::getStatusColors()[$user->status][0]}}"
                                    >{{\App\Models\User::getStatuses()[$user->status]}}</span>
                                    <span class="btn btn-primary btn-xs float-right" data-toggle="modal" data-target="#change_status_btn" data-user="{{$user->id}}" data-status="{{$user->status}}">Змінити</span>
                                </td>
                                <td>
                                    <div style="display: flex">
                                        <div style="margin-left: 10px">
                                            <form action="{{ route('users.destroy', $user->id) }}" method="POST">

                                                <a href="{{ route('users.show', $user->id) }}" title="show" class="btn btn-primary btn-xs">
                                                    <i class="fas fa-eye"></i>
                                                </a>

                                                {{--                                            <a href="{{ route('users.edit', $user->id) }}">--}}
                                                {{--                                                <i class="fas fa-edit  fa-lg"></i>--}}
                                                {{--                                            </a>--}}

                                                @csrf
                                                @method('DELETE')

                                                &nbsp;
                                                <a href="javascript:void(0)" title="Delete" class="btn btn-danger btn-xs delete-item-btn">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $users->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(() => {
            $('.delete-item-btn').on('click',function() {
                if(confirm('You definitely want to delete the user?')){
                    $(this).closest('form').submit();
                }
            });
        });
    </script>
@endpush
