@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item" aria-current="page">Настройки</li>
            <li class="breadcrumb-item active" aria-current="page">{{\App\Models\Settings::getTabNames()[$tab]}}</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <i class="mdi mdi-settings"></i>
                    <span style="text-transform: uppercase">{{\App\Models\Settings::getTabNames()[$tab]}}</span>
                </div>
                <div class="card-body">
                    <form action="{{route('settings.save')}}" method="post" class="form-horizontal">
                        @csrf
                        @include('admin.settings.tabs.' . $tab, ['data' => $data])

                        <input type="submit" value="Сохранить" class="btn btn-success text-white float-right">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{asset('assets/plugins/select2/css/select2.min.css')}}">
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #5897fb !important;
            border: 1px solid #5897fb !important;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            color: #fff !important;
        }

        .select2-container--classic .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            background-color:#5897fb !important;
        }

        .select2-container--default .select2-selection--multiple {
            border: 1px solid #e9ecef;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 1px solid #e9ecef;
            color: #3e5569;
            background-color: #fff;
            border-color: rgba(0,0,0,0.25);
            outline: 0;
            box-shadow: 0 0 0 0.2rem rgb(0 123 255 / 25%);
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__display {
            padding-left: 15px;
        }
    </style>
@endpush

@push('scripts')
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $(".select2-field").select2();
        });
    </script>
@endpush
