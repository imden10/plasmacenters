<ul class="nav nav-tabs" role="tablist">
    @foreach($localizations as $key => $lang)
        <li class="nav-item">
            <a class="nav-link @if(app()->getLocale() == $key) active @endif"
               data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                <span class="hidden-sm-up"></span> <span
                    class="hidden-xs-down">{{ $lang }}</span>
            </a>
        </li>
    @endforeach
</ul>

<br>

<div class="tab-content">
    @foreach($localizations as $key => $catLang)
        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif"
             id="main_lang_{{ $key }}" role="tabpanel"
        >
            <h4>Заголовок страницы</h4>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_donors_page_name_{{ $key }}">Заголовок страницы</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][donors_page_name]" value="{{ old('setting_data.' . $key . '.donors_page_name', $data[$key]['donors_page_name'][0]['value'] ?? '') }}" id="setting_donors_page_name_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.donors_page_name') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <hr>


            <h4>Meta данные</h4>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_donors_title_{{ $key }}">Заголовок</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][donors_title]" value="{{ old('setting_data.' . $key . '.donors_title', $data[$key]['donors_title'][0]['value'] ?? '') }}" id="setting_donors_title_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.donors_title') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_donors_description_{{ $key }}">Описание</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][donors_description]" value="{{ old('setting_data.' . $key . '.donors_description', $data[$key]['donors_description'][0]['value'] ?? '') }}" id="setting_donors_description_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.donors_description') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <hr>
        </div>
    @endforeach
</div>


<?php $defaultLang = \App\Models\Langs::getDefaultLangCode();?>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_donors_menu_{{ $defaultLang }}">Меню для страницы ВСЕХ доноров</label>
    <div class="col-md-9">
        <select name="setting_data[{{ $defaultLang }}][donors_menu]" class="select2-field" id="setting_donors_menu_{{ $defaultLang }}">
            <option value="">---</option>
            @foreach(\App\Models\Menu::getTagsWithId() as $menuId => $item)
                <option value="{{$menuId}}" @if(old('setting_data.' . $defaultLang . '.donors_menu', $data[$defaultLang]['donors_menu'][0]['value'] ?? '') == $menuId) selected @endif>{{$item}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_donors_default_page_menu_{{ $defaultLang }}">Меню по умолчанию, для страницы ОДНОГО донора</label>
    <div class="col-md-9">
        <select name="setting_data[{{ $defaultLang }}][donors_default_page_menu]" class="select2-field" id="setting_donors_default_page_menu_{{ $defaultLang }}">
            <option value="">---</option>
            @foreach(\App\Models\Menu::getTagsWithId() as $menuId => $item)
                <option value="{{$menuId}}" @if(old('setting_data.' . $defaultLang . '.donors_default_page_menu', $data[$defaultLang]['donors_default_page_menu'][0]['value'] ?? '') == $menuId) selected @endif>{{$item}}</option>
            @endforeach
        </select>
    </div>
</div>
