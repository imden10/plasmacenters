<ul class="nav nav-tabs" role="tablist">
    @foreach($localizations as $key => $lang)
        <li class="nav-item">
            <a class="nav-link @if(app()->getLocale() == $key) active @endif"
               data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                <span class="hidden-sm-up"></span> <span
                    class="hidden-xs-down">{{ $lang }}</span>
            </a>
        </li>
    @endforeach
</ul>

<br>

<div class="tab-content">
    @foreach($localizations as $key => $catLang)
        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif"
             id="main_lang_{{ $key }}" role="tabpanel"
        >

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_copyright_{{ $key }}">Копирайт</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][copyright]" value="{{ old('setting_data.' . $key . '.copyright', $data[$key]['copyright'][0]['value'] ?? '') }}" id="setting_copyright_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.copyright') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_sing_link_{{ $key }}">Ссылка на форму записи</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][sing_link]" value="{{ old('setting_data.' . $key . '.sing_link', $data[$key]['sing_link'][0]['value'] ?? '') }}" id="setting_sing_link_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.sing_link') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <h4>Скрытая форма</h4>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_form_title_{{ $key }}">Заголовок</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][form_title]" value="{{ old('setting_data.' . $key . '.form_title', $data[$key]['form_title'][0]['value'] ?? '') }}" id="setting_form_title_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.form_title') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_form_subtitle_{{ $key }}">Подзаголовок</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][form_subtitle]" value="{{ old('setting_data.' . $key . '.form_subtitle', $data[$key]['form_subtitle'][0]['value'] ?? '') }}" id="setting_form_subtitle_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.form_subtitle') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <hr>
        </div>
    @endforeach
</div>


<?php $defaultLang = \App\Models\Langs::getDefaultLangCode();?>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_footer_menu_{{ $defaultLang }}">Меню в подвале</label>
    <div class="col-md-9">
        <select name="setting_data[{{ $defaultLang }}][footer_menu]" class="select2-field" id="setting_footer_menu_{{ $defaultLang }}">
            <option value="">---</option>
            @foreach(\App\Models\Menu::getTagsWithId() as $menuId => $item)
                <option value="{{$menuId}}" @if(old('setting_data.' . $defaultLang . '.footer_menu', $data[$defaultLang]['footer_menu'][0]['value'] ?? '') == $menuId) selected @endif>{{$item}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_default_og_image_{{ $defaultLang }}">Логотип сайта</label>
    <div class="col-md-9">
        {{ media_preview_box("setting_data[".$defaultLang."][default_og_image]",old('setting_data.' . $defaultLang . '.default_og_image', $data[$defaultLang]['default_og_image'][0]['value'] ?? '')) }}
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_head_code_{{ $defaultLang }}">Код в HEAD</label>
    <div class="col-md-9">
        <textarea name="setting_data[{{ $defaultLang }}][head_code]" id="setting_head_code_{{ $defaultLang }}" cols="30" rows="10" class="form-control">{{old('setting_data.' . $defaultLang . '.head_code', $data[$defaultLang]['head_code'][0]['value'] ?? '')}}</textarea>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_body_code_{{ $defaultLang }}">Код в конец BODY</label>
    <div class="col-md-9">
        <textarea name="setting_data[{{ $defaultLang }}][body_code]" id="setting_body_code_{{ $defaultLang }}" cols="30" rows="10" class="form-control">{{old('setting_data.' . $defaultLang . '.body_code', $data[$defaultLang]['body_code'][0]['value'] ?? '')}}</textarea>
    </div>
</div>




