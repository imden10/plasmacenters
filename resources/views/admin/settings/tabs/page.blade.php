<?php $defaultLang = \App\Models\Langs::getDefaultLangCode();?>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_page_default_page_menu_{{ $defaultLang }}">Меню по умолчанию, для ОДНОЙ страницы</label>
    <div class="col-md-9">
        <select name="setting_data[{{ $defaultLang }}][page_default_page_menu]" class="select2-field" id="setting_page_default_page_menu_{{ $defaultLang }}">
            <option value="">---</option>
            @foreach(\App\Models\Menu::getTagsWithId() as $menuId => $item)
                <option value="{{$menuId}}" @if(old('setting_data.' . $defaultLang . '.page_default_page_menu', $data[$defaultLang]['page_default_page_menu'][0]['value'] ?? '') == $menuId) selected @endif>{{$item}}</option>
            @endforeach
        </select>
    </div>
</div>
