<div class="social-elem">
    <div class="l">
        <input type="text" name="socials[{{$randKey}}][link]" placeholder="Ссылка" value="{{$link}}">
        <select type="text" name="socials[{{$randKey}}][icon]">
            <option value="ic-facebook" @if($icon == 'ic-facebook') selected @endif>Facebook</option>
            <option value="ic-instagram"  @if($icon == 'ic-instagram') selected @endif>Instagram</option>
        </select>
    </div>
    <div class="r">
        <span class="remove-btn" title="Удалить">
            <i class="fa fa-times"></i>
        </span>
    </div>
</div>
