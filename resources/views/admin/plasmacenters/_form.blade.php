<div class="form-group row">
    <label class="col-md-3 text-right" for="page_sort">Сортировка</label>
    <div class="col-md-9">
        <input type="text" name="sort" value="{{ old('sort', $model->sort ?? '') }}" id="page_sort" class="form-control{{ $errors->has('sort') ? ' is-invalid' : '' }}">

        @if ($errors->has('sort'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('sort') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_tag">Тег</label>
    <div class="col-md-9">
        <input type="text" name="tag" value="{{ old('tag', $model->tag ?? '') }}" id="page_tag" class="form-control{{ $errors->has('tag') ? ' is-invalid' : '' }}">

        @if ($errors->has('tag'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('tag') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_request_link">Ссылка для записи</label>
    <div class="col-md-9">
        <input type="text" name="request_link" value="{{ old('request_link', $model->request_link ?? '') }}" id="page_request_link" class="form-control{{ $errors->has('request_link') ? ' is-invalid' : '' }}">

        @if ($errors->has('request_link'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('request_link') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_coord_lat">Координаты lat</label>
    <div class="col-md-9">
        <input type="text" name="coord_lat" value="{{ old('coord_lat', $model->coord_lat ?? '') }}" id="page_coord_lat" class="form-control{{ $errors->has('coord_lat') ? ' is-invalid' : '' }}">

        @if ($errors->has('coord_lat'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('coord_lat') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_coord_lng">Координаты lng</label>
    <div class="col-md-9">
        <input type="text" name="coord_lng" value="{{ old('coord_lng', $model->coord_lng ?? '') }}" id="page_coord_lng" class="form-control{{ $errors->has('coord_lng') ? ' is-invalid' : '' }}">

        @if ($errors->has('coord_lng'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('coord_lng') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_menu_id">Меню</label>
    <div class="col-md-9">
        <select name="menu_id" id="menu_id" class="select-field" style="width: 100%">
            <option value="">---</option>
            @foreach(\App\Models\Menu::getTagsWithId() as $menuId => $item)
                <option value="{{$menuId}}" @if(old('menu_id', $model->menu_id ?? '') == $menuId) selected @endif>{{$item}}</option>
            @endforeach
        </select>

        @if ($errors->has('menu_id'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('menu_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_slug">Slug</label>
    <div class="col-md-9">
        <input type="text" name="slug" value="{{ old('slug', $model->slug ?? '') }}" id="page_slug" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}">

        @if ($errors->has('slug'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('slug') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_is_share_block">Отображать блок "Поделиться"</label>
    <div class="col-md-9">
        <div class="material-switch pull-left">
            <input id="someSwitchOptionSuccess_is_share_block" name="is_share_block" value="1" type="checkbox" {{ old('is_share_block', $model->is_share_block) ? ' checked' : '' }}/>
            <label for="someSwitchOptionSuccess_is_share_block" class="label-success"></label>
        </div>

        @if ($errors->has('is_share_block'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('is_share_block') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_is_sign_up_block">Отображать блок "Записаться на донацию"</label>
    <div class="col-md-9">
        <div class="material-switch pull-left">
            <input id="someSwitchOptionSuccess_is_sign_up_block" name="is_sign_up_block" value="1" type="checkbox" {{ old('is_sign_up_block', $model->is_sign_up_block) ? ' checked' : '' }}/>
            <label for="someSwitchOptionSuccess_is_sign_up_block" class="label-success"></label>
        </div>

        @if ($errors->has('is_sign_up_block'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('is_sign_up_block') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_status">Статус</label>
    <div class="col-md-9">
        <div class="material-switch pull-left">
            <input id="someSwitchOptionSuccess" name="status" value="1" type="checkbox" {{ old('status', $model->status) ? ' checked' : '' }}/>
            <label for="someSwitchOptionSuccess" class="label-success"></label>
        </div>

        @if ($errors->has('status'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.select-field').select2();
        })
    </script>
@endpush
