@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item"><a href="{{route('plasmacenters.index')}}">Плазмацентры</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактировать</li>
        </ol>
    </nav>

    <div style="display: flex; justify-content: flex-end;margin-bottom: 10px;">
        <a href="/plasmacenter/{{$model->slug}}" target="_blank" title="Посмотреть на сайте" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Посмотреть на сайте</a>
    </div>

    <form class="form-horizontal" method="POST" action="{{route('plasmacenters.update', $model->id)}}">
        @method('PUT')
        @csrf

    <div class="row">
        <div class="col-md-12">
            <div class="card">

            <div class="card-header">
                <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-bottom: -10px;border-bottom: none">
                    <li class="nav-item">
                        <a class="nav-link active" id="main-tab" data-toggle="tab" href="#main" role="tab" aria-controls="main" aria-selected="true">Головне</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="seo-tab" data-toggle="tab" href="#seo" role="tab" aria-controls="seo" aria-selected="false">SEO</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="socials-tab" data-toggle="tab" href="#socials" role="tab" aria-controls="socials" aria-selected="false">Соц. сети</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="audio-tab" data-toggle="tab" href="#audio" role="tab" aria-controls="audio" aria-selected="false">Аудио треки</a>
                    </li>
                </ul>
            </div>

             <div class="card-body">
                 <div class="tab-content" id="myTabContent">
                     {{----------------------------- MAIN TAB -----------------------------------------}}
                     <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="main-tab">
                         <ul class="nav nav-tabs" role="tablist">
                             @foreach($localizations as $key => $lang)
                                 <li class="nav-item">
                                     <a class="nav-link @if(app()->getLocale() == $key) active @endif" data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                                         <span class="hidden-sm-up"></span> <span class="hidden-xs-down">{{ $lang }}</span>
                                     </a>
                                 </li>
                             @endforeach
                         </ul>

                         <br>
                         <div class="tab-content">
                             @foreach($localizations as $key => $catLang)
                                 <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif" id="main_lang_{{ $key }}" role="tabpanel">
                                     @include('admin.plasmacenters.tabs._main',[
                                        'lang' => $key,
                                        'data' => $data,
                                        'model' => $model
                                     ])
                                     {!! Constructor::output($model->getTranslation($key),$key) !!}
                                 </div>
                             @endforeach

                             @include('admin.plasmacenters._form',['model' => $model])
                         </div>
                     </div>

                     {{----------------------------- SEO TAB -----------------------------------------}}
                     <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">
                         <ul class="nav nav-tabs" role="tablist">
                             @foreach($localizations as $key => $lang)
                                 <li class="nav-item">
                                     <a class="nav-link @if(app()->getLocale() == $key) active @endif" data-toggle="tab" href="#seo_lang_{{ $key }}" role="tab">
                                         <span class="hidden-sm-up"></span> <span class="hidden-xs-down">{{ $lang }}</span>
                                     </a>
                                 </li>
                             @endforeach
                         </ul>

                         <br>
                         <div class="tab-content tabcontent-border">
                             @foreach($localizations as $key => $catLang)
                                 <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif" id="seo_lang_{{ $key }}" role="tabpanel">
                                     @include('admin.plasmacenters.tabs._seo',[
                                        'lang' => $key,
                                        'data' => $data
                                     ])
                                 </div>
                             @endforeach
                         </div>
                     </div>

                     {{----------------------------- Soc TAB -----------------------------------------}}
                     <div class="tab-pane fade" id="socials" role="tabpanel" aria-labelledby="socials-tab">
                         @include('admin.plasmacenters.tabs._socials',[
                                        'lang' => $key,
                                        'data' => $data
                                     ])
                     </div>

                     {{----------------------------- Аудио TAB -----------------------------------------}}
                     <div class="tab-pane fade" id="audio" role="tabpanel" aria-labelledby="audio-tab">
                         @include('admin.plasmacenters.tabs._audio',[
                                        'lang' => $key,
                                        'data' => $data
                                     ])
                     </div>
                 </div>

                 <input type="submit" value="Сохранить" class="btn btn-success text-white float-right">

             </div>
            </div>
        </div>
    </div>

    </form>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{asset('assets/plugins/select2/css/select2.min.css')}}">
    <style>
        /*-- ==============================================================
        Switches
        ============================================================== */
        .material-switch {
            line-height: 3em;
        }
        .material-switch > input[type="checkbox"] {
            display: none;
        }

        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }

        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #5897fb !important;
            border: 1px solid #5897fb !important;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            color: #fff !important;
        }

        .select2-container--classic .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            background-color:#5897fb !important;
        }

        .select2-container--default .select2-selection--multiple {
            border: 1px solid #e9ecef;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 1px solid #e9ecef;
            color: #3e5569;
            background-color: #fff;
            border-color: rgba(0,0,0,0.25);
            outline: 0;
            box-shadow: 0 0 0 0.2rem rgb(0 123 255 / 25%);
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__display {
            padding-left: 15px;
        }
    </style>
@endpush

@push('scripts')
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $(".select2-field").select2();

            $(document).on('click','.remove-btn',function () {
                (this).closest('.menu-elem').remove();
            })
        });
    </script>
@endpush
