<h4>Добавьте соц. сети</h4>

<span class="btn btn-primary add-social-elem">Добавить</span>

<div class="socials-container connectedSortable" id="sortable">
    @if(count($socials))
        @foreach($socials as $socialKey => $item)
            @include('admin.plasmacenters._social_elem',[
                'link'    => $item['link'],
                'icon'    => $item['icon'],
                'randKey' => $socialKey
        ])
        @endforeach
    @endif
</div>

@push('styles')
    <style>
        .socials-container {
            margin: 30px 0;
            padding: 3px 6px;
            border: 1px #cccccc4d solid;
            min-height: 40px;
        }

        .socials-container .social-elem {
            border: 1px #ccc solid;
            padding: 5px 10px;
            display: flex;
            justify-content: space-between;
            align-items: center;
            cursor: move;
            margin: 3px 0;
        }

        .socials-container .social-elem .l input {
            display: inline-block;
        }

        .socials-container .social-elem .r .remove-btn {
            width: 30px;
            display: inline-flex;
            height: 30px;
            align-items: center;
            justify-content: center;
            margin: -5px -10px -5px -10px;
            cursor: pointer;
            transition: all .3s;
        }

        .socials-container .social-elem .r .remove-btn:hover {
            transform: scale(1.1);
            transition: all .3s;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $(document).ready(function () {
            $(".add-social-elem").on('click', function (e) {
                $.ajax({
                    url:"{{route('plasmacenters.add-social')}}",
                    type:"post",
                    data:{
                        _token:"{{csrf_token()}}"
                    },
                    success:function(data){
                        if(data !== ''){
                            $(".socials-container").append(data);
                        }
                    }
                });
            });

            $( "#sortable" ).sortable({
                connectWith: ".connectedSortable",
                stop: function(event, ui) {
                    $('.connectedSortable').each(function() {
                        result = "";
                        $(this).find("li").each(function(){
                            result += $(this).text() + ",";
                        });
                        $("."+$(this).attr("id")+".list").html(result);
                    });
                }
            });

            $(document).on('click','.remove-btn',function () {
                (this).closest('.social-elem').remove();
            })
        });
    </script>
@endpush
