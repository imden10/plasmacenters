<div class="form-group row">
    <label class="col-md-3 text-right" for="page_title_{{ $lang }}">Название</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][title]" value="{{ old('page_data.' . $lang . '.title', $data[$lang]['title'] ?? '') }}" id="page_title_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.title') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.title'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.title') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_city_{{ $lang }}">Город</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][city]" value="{{ old('page_data.' . $lang . '.city', $data[$lang]['city'] ?? '') }}" id="page_city_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.city') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.city'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.city') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_address_{{ $lang }}">Адрес</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][address]" value="{{ old('page_data.' . $lang . '.address', $data[$lang]['address'] ?? '') }}" id="page_address_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.address') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.address'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.address') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_schedule_{{ $lang }}">Расписание</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][schedule]" value="{{ old('page_data.' . $lang . '.schedule', $data[$lang]['schedule'] ?? '') }}" id="page_schedule_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.schedule') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.schedule'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.schedule') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_email_{{ $lang }}">E-mail</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][email]" value="{{ old('page_data.' . $lang . '.email', $data[$lang]['email'] ?? '') }}" id="page_email_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.email') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.email') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_phone_{{ $lang }}">Телефон</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][phone]" value="{{ old('page_data.' . $lang . '.phone', $data[$lang]['phone'] ?? '') }}" id="page_phone_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.phone') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.phone'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.phone') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_description_{{ $lang }}">Описание</label>
    <div class="col-md-9">
        <textarea
            name="page_data[{{ $lang }}][description]"
            id="page_description_{{ $lang }}"
            class="summernote editor {{ $errors->has('page_data.' . $lang . '.description') ? ' is-invalid' : '' }}"
            cols="30"
            rows="10"
        >{{ old('page_data.' . $lang . '.description', $data[$lang]['description'] ?? '') }}</textarea>

        @if ($errors->has('page_data.' . $lang . '.description'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.description') }}</strong>
            </span>
        @endif
    </div>
</div>

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(() => {
            // $('.select2').select2();
        });
    </script>
@endpush
