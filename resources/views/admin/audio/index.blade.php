@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item active" aria-current="page">Аудио треки</li>
        </ol>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <a href="{{route('audio.create')}}" class="btn btn-primary float-right">
                    <i class="fa fa-plus"></i>
                    Добавить
                </a>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($model as $item)
                            <tr>
                                <td>{{$item->name}}</td>
                                <td>
                                    <form action="{{ route('audio.destroy', $item->id) }}" method="POST">

                                        <a href="{{ route('audio.edit', $item->id) }}" class="btn btn-xs btn-primary">
                                            <i class="fas fa-edit fa-lg"></i>
                                        </a>

                                        @csrf
                                        @method('DELETE')

                                        <a href="javascript:void(0)" title="Удалить" class="btn btn-danger btn-xs delete-item-btn text-white">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $model->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(() => {
            $('.delete-item-btn').on('click',function() {
                if(confirm('Вы уверены, что хотите удалить запись?')){
                    $(this).closest('form').submit();
                }
            });
        });
    </script>
@endpush

