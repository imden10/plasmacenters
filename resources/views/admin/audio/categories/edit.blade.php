@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item">Аудио треки</li>
            <li class="breadcrumb-item"><a href="{{route('audio-categories.index')}}">Категории</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактировать</li>
        </ol>
    </nav>

    <form class="form-horizontal" method="POST" action="{{route('audio-categories.update', $model->id)}}">
        @csrf
        @method('PUT')

        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        <ul class="nav nav-tabs" role="tablist">
                            @foreach($localizations as $key => $lang)
                                <li class="nav-item">
                                    <a class="nav-link @if(app()->getLocale() == $key) active @endif"
                                       data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                                        <span class="hidden-sm-up"></span> <span
                                            class="hidden-xs-down">{{ $lang }}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>

                        <br>
                        <div class="tab-content">
                            @foreach($localizations as $key => $catLang)
                                <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif"
                                     id="main_lang_{{ $key }}" role="tabpanel">
                                    @include('admin.audio.categories._form',[
                                       'lang'  => $key,
                                       'model' => $model,
                                       'data'  => $data
                                    ])
                                </div>
                            @endforeach
                        </div>

                        <input type="submit" value="Сохранить" class="btn btn-success text-white float-right">

                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection
