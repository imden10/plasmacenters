@extends('layouts.admin.app')

@section('content')
    @include('admin.request._modal_change_status')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item active" aria-current="page">Онлайн записи</li>
        </ol>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card card-exportable">
            <div class="card-header">
{{--                <a href="{{route('request.create')}}" class="btn btn-primary float-right" style="margin-left: 10px">--}}
{{--                    <i class="fa fa-plus"></i>--}}
{{--                    Добавить--}}
{{--                </a>--}}
            </div>
            <div class="card-body">
                <form action="" method="get">
                    <div class="form-row">
                        <div class="form-group col-md-1">
                            <label>Источник</label>
                            <select name="promo" class="select2 form-control m-t-15">
                                <option value="">---</option>
                                    <option value="1" @if(old('promo', request()->input('promo')) == '1') selected @endif>Promo 1</option>
                                    <option value="2" @if(old('promo', request()->input('promo')) == '2') selected @endif>Promo 2</option>
                            </select>
                        </div>

                        <div class="form-group col-md-2">
                            <label>Дата заявки</label>
                            <?php
                                $dateFrom = $from;
                                $dateTo = $to;
                                $date = request()->input('p');
                                if($date) {
                                    $dateFrom = substr($date,0,10);
                                    $dateTo = substr($date,13,10);
                                }
                            ?>
                            <input type="text" name="p" class="form-control" style="width: 200px;">
                        </div>

                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <button type="submit" class="btn btn-success form-control text-white filter-btn-apply" title="Филтрофать">
                                <i class="fa fa-filter"></i>
                            </button>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <a href="{{ route('request.index') }}" class="btn btn-danger form-control text-white" title="Сбросить">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-clockwise" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd" d="M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2v1z"/>
                                    <path d="M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466z"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </form>

                <table class="table table-bordered imSortingTableLib">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th class="sorting @if(request()->input('sort') == 'promo' && request()->input('order') == 'asc') sorting_asc @elseif(request()->input('sort') == 'promo' && request()->input('order') == 'desc') sorting_desc @endif" data-field="promo">Источник</th>
                            <th data-field="phone">Телефон</th>
                            <th class="sorting @if(request()->input('sort') == 'plasmacenter_name' && request()->input('order') == 'asc') sorting_asc @elseif(request()->input('sort') == 'plasmacenter_name' && request()->input('order') == 'desc') sorting_desc @endif" data-field="plasmacenter_name">Город</th>
                            <th class="sorting @if(request()->input('sort') == 'created_at' && request()->input('order') == 'asc') sorting_asc @elseif(request()->input('sort') == 'created_at' && request()->input('order') == 'desc') sorting_desc @endif" data-field="created_at">Дата заявки</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($model as $item)
                            <tr data-id="{{$item->id}}">
                                <td>{{$item->id}}</td>
                                <td>Promo {{$item->promo}}</td>{{-- todo принимать полностью строку с фронта "промо 1"--}}
                                <td>{{$item->phone}}</td>
                                <td>{{$item->plasmacenter_name}}</td>
                                <td>{{$item->created_at->format('d-m-Y H:i')}}</td>
                                <td>
                                    <div style="display: flex">
                                        <div style="margin-left: 10px">
                                            <form action="{{ route('request.destroy', $item->id) }}" method="POST">

{{--                                                <a href="{{ route('request.edit', $item->id) }}" class="btn btn-xs btn-primary">--}}
{{--                                                    <i class="fas fa-edit fa-lg"></i>--}}
{{--                                                </a>--}}

                                                @csrf
                                                @method('DELETE')

                                                <a href="javascript:void(0)" title="Удалить" class="btn btn-danger btn-xs delete-item-btn text-white">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $model->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style>
        table.imSortingTableLib th.sorting {
            position: relative;
        }

        table.imSortingTableLib th.sorting::before {
            right: 1em;
            content: "\2191";
            opacity: 0.5;
            position: absolute;
        }

        table.imSortingTableLib th.sorting::after {
            right: 0.5em;
            content: "\2193";
            opacity: 0.5;
            position: absolute;
        }

        table.imSortingTableLib th.sorting.sorting_asc::before {
            opacity: 1;
        }

        table.imSortingTableLib th.sorting.sorting_desc::after {
            opacity: 1;
        }
    </style>
@endpush

@push('scripts')
    <script src="{{asset('/myLib/im-export-table-lib.js')}}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
        function insertParam(key, value,url = null) {
            key = encodeURIComponent(key);
            value = encodeURIComponent(value);

            if(url){
                var kvp = url.substr(1).split('&');
            } else {
                var kvp = document.location.search.substr(1).split('&');
            }
            let i=0;

            for(; i<kvp.length; i++){
                if (kvp[i].startsWith(key + '=')) {
                    let pair = kvp[i].split('=');
                    pair[1] = value;
                    kvp[i] = pair.join('=');
                    break;
                }
            }

            if(i >= kvp.length){
                kvp[kvp.length] = [key,value].join('=');
            }

            // can return this or...
            let params = kvp.join('&');

            return params;
        }

        $(document).ready(() => {
            let exportLib = new imExportTableLib({
                cardQuerySelector: ".card-exportable",
                dbTable: "requests"
            });

            $('.delete-item-btn').on('click',function() {
                if(confirm('Вы пытаетесь удалить запись?')){
                    $(this).closest('form').submit();
                }
            });

            $('input[name="p"]').daterangepicker({
                opens: 'left',
                autoApply: true,
                locale: {
                    format: 'DD-MM-YYYY',
                    customRangeLabel: "Произвольный диапазон",
                    daysOfWeek: [
                        "Вс",
                        "Пн",
                        "Вт",
                        "Ср",
                        "Чт",
                        "Пт",
                        "Сб"
                    ],
                    monthNames: [
                        "Январь",
                        "Февраль",
                        "Март",
                        "Апрель",
                        "Май",
                        "Июнь",
                        "Июль",
                        "Август",
                        "Сентябрь",
                        "Октябрь",
                        "Ноябрь",
                        "Декабрь"
                    ],
                    firstDay: 1
                },
                startDate: "{{$dateFrom}}",
                endDate: "{{$dateTo}}",
                alwaysShowCalendars: true,
                ranges: {
                    'Сегодня': [moment(), moment()],
                    'Вчерва': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
                    'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
                    'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
                    'Прошлый месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });

            $('input[name="p"]').on('apply.daterangepicker', function(ev, picker) {
                $(".filter-btn-apply").trigger('click');
            });
            
            $("#export_form").submit(function () {
                let date = $("input[name='p']").val();
                let promo =$("select[name='promo']").val();

                let params = {};

                if(date){
                    params['date'] = date;
                }

                if(promo){
                    params['promo'] = promo;
                }

                let input = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "params").val(JSON.stringify(params));
                $('#export_form').append(input);
            });

            /* SORTING*/
            $(document).on('click',"table.imSortingTableLib th.sorting",function () {
                $("table.imSortingTableLib th.sorting").removeClass('sorting_asc');
                $("table.imSortingTableLib th.sorting").removeClass('sorting_desc');
                $(this).addClass('sorting_asc');

                let field = $(this).data('field');
                let url = insertParam('sort',field);
                url = insertParam('order','asc','?' + url);

                document.location.search = url;
            });

            $(document).on('click',"table.imSortingTableLib th.sorting_asc",function () {
                $("table.imSortingTableLib th.sorting").removeClass('sorting_asc');
                $("table.imSortingTableLib th.sorting").removeClass('sorting_desc');
                $(this).addClass('sorting_desc');

                let field = $(this).data('field');
                let url = insertParam('sort',field);
                url = insertParam('order','desc','?' + url);

                document.location.search = url;
            });

            $(document).on('click',"table.imSortingTableLib th.sorting_desc",function () {
                $("table.imSortingTableLib th.sorting").removeClass('sorting_asc');
                $("table.imSortingTableLib th.sorting").removeClass('sorting_desc');
                $(this).addClass('sorting_asc');

                let field = $(this).data('field');
                let url = insertParam('sort',field);
                url = insertParam('order','asc','?' + url);

                document.location.search = url;
            });
        });
    </script>
@endpush
