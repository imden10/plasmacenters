<div class="form-group row">
    <label class="col-md-3 text-right" for="page_name">Имя</label>
    <div class="col-md-9">
        <input type="text" name="name" value="{{ old('name', $model->name ?? '') }}" id="page_name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}">

        @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_phone">Телефон</label>
    <div class="col-md-9">
        <input type="text" name="phone" value="{{ old('phone', $model->phone ?? '') }}" id="page_phone" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}">

        @if ($errors->has('phone'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_plasmacenter_id">Плазмацентр</label>
    <div class="col-md-9">
        <select name="plasmacenter_id" id="plasmacenter_id" class="select-field">
            <option value="">---</option>
            @foreach(\App\Models\Plasmacenters::query()->active()->get() as $item)
                <option value="{{$item->id}}" @if(old('plasmacenter_id', $model->plasmacenter_id ?? '') === $item->id) selected @endif>{{$item->title}}</option>
            @endforeach
        </select>

        @if ($errors->has('plasmacenter_id'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('plasmacenter_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="datetime">
        Дата записи</label>
    <div class="col-md-3">
        <input type="input" class="form-control" name="datetime" value="{{$model->datetime}}" id="datetime">
    </div>
</div>

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('matrix/css/icons/font-awesome/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <style>
        /*-- ==============================================================
        Switches
        ============================================================== */
        .material-switch {
            line-height: 3em;
        }
        .material-switch > input[type="checkbox"] {
            display: none;
        }

        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }

        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }
    </style>
@endpush

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="{{asset('matrix/libs/moment/moment.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        $(document).ready(() => {
            $('.select-field').select2();

            $('#datetime').datetimepicker({
                format: 'DD-MM-YYYY HH:mm',
                useCurrent: true,
                icons: {
                    time: "fa fa-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down",
                    previous: "fa fa-chevron-left",
                    next: "fa fa-chevron-right",
                    today: "fa fa-clock",
                    clear: "fa fa-trash"
                }
            });
        });
    </script>
@endpush
