<div id="change_status_btn" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form action="{{route('request.status-change')}}" method="POST">
            <input type="hidden" name="request_id" id="request_id">
        @csrf
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <select name="status" class="form-control" id="select_status">
                    @foreach(\App\Models\Request::getStatuses() as $key => $status)
                        <option value="{{$key}}">{{$status}}</option>
                    @endforeach
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default text-white" data-dismiss="modal">Закрыть</button>
                <input type="submit" class="btn btn-success text-white" value="Сменить">
            </div>
        </div>
        </form>
    </div>
</div>

@push('scripts')
    <script>
        $('#change_status_btn').on('show.bs.modal', function(e) {
            let request = e.relatedTarget.dataset.request;
            let status = e.relatedTarget.dataset.status;
            $("#request_id").val(request);
            $("#select_status").val(status);
        });
    </script>

@endpush
