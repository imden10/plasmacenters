@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item active" aria-current="page">Частые вопросы</li>
        </ol>
        <a href="{{route('faq.create')}}" class="btn btn-primary">
            <i class="fa fa-plus"></i>
            Добавить
        </a>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Вопрос</th>
                        <th>Ответ</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($model as $item)
                            <tr>
                                <td>{{$item->question}}</td>
                                <td>{{ mb_strimwidth(strip_tags($item->answer), 0, 100, "...") }}</td>
                                <td>
                                    <form action="{{ route('faq.destroy', $item->id) }}" method="POST">

                                        <a href="{{ route('faq.edit', $item->id) }}" class="btn btn-xs btn-primary">
                                            <i class="fas fa-edit fa-lg"></i>
                                        </a>

                                        @csrf
                                        @method('DELETE')

                                        <a href="javascript:void(0)" title="Видалити" class="btn btn-danger btn-xs delete-item-btn text-white">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $model->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <style>
        nav.breadcrumb-nav {
            position: relative;
        }
        nav.breadcrumb-nav a.btn {
            position: absolute;
            right: 15px;
            top: 4px;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $(document).ready(() => {
            $('.delete-item-btn').on('click',function() {
                if(confirm('Ви впевнені, що хочите видалити певний запис?')){
                    $(this).closest('form').submit();
                }
            });
        });
    </script>
@endpush

