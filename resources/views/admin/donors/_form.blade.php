<div class="form-group row">
    <label class="col-md-3 text-right" for="name">Изображение</label>
    <div class="col-md-9">
        {{ media_preview_box('image',$model->image) }}
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_youtube">Ссылка на видео Youtube</label>
    <div class="col-md-9">
        <input type="text" name="youtube" value="{{ old('youtube', $model->youtube ?? '') }}" id="page_youtube" class="form-control{{ $errors->has('youtube') ? ' is-invalid' : '' }}">

        @if ($errors->has('youtube'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('youtube') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right"></label>
    <div class="col-md-9">
        <img src="" alt="" class="youtube-preview" style="max-width: 200px; border-radius: 7px">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_menu_id">Меню</label>
    <div class="col-md-9">
        <select name="menu_id" id="menu_id" class="select-field" style="width: 100%">
            <option value="">---</option>
            @foreach(\App\Models\Menu::getTagsWithId() as $menuId => $item)
                <option value="{{$menuId}}" @if(old('menu_id', $model->menu_id ?? '') == $menuId) selected @endif>{{$item}}</option>
            @endforeach
        </select>

        @if ($errors->has('menu_id'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('menu_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_donations">Количество донаций</label>
    <div class="col-md-9">
        <input type="text" name="donations" value="{{ old('donations', $model->donations ?? '') }}" id="page_donations" class="form-control{{ $errors->has('donations') ? ' is-invalid' : '' }}">

        @if ($errors->has('donations'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('donations') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_slug">Slug</label>
    <div class="col-md-9">
        <input type="text" name="slug" value="{{ old('slug', $model->slug ?? '') }}" id="page_slug" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}">

        @if ($errors->has('slug'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('slug') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_is_share_block">Отображать блок "Поделиться"</label>
    <div class="col-md-9">
        <div class="material-switch pull-left">
            <input id="someSwitchOptionSuccess_is_share_block" name="is_share_block" value="1" type="checkbox" {{ old('is_share_block', $model->is_share_block) ? ' checked' : '' }}/>
            <label for="someSwitchOptionSuccess_is_share_block" class="label-success"></label>
        </div>

        @if ($errors->has('is_share_block'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('is_share_block') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_is_sign_up_block">Отображать блок "Записаться на донацию"</label>
    <div class="col-md-9">
        <div class="material-switch pull-left">
            <input id="someSwitchOptionSuccess_is_sign_up_block" name="is_sign_up_block" value="1" type="checkbox" {{ old('is_sign_up_block', $model->is_sign_up_block) ? ' checked' : '' }}/>
            <label for="someSwitchOptionSuccess_is_sign_up_block" class="label-success"></label>
        </div>

        @if ($errors->has('is_sign_up_block'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('is_sign_up_block') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_status">Статус</label>
    <div class="col-md-9">
        <div class="material-switch pull-left">
            <input id="someSwitchOptionSuccess" name="status" value="1" type="checkbox" {{ old('status', $model->status) ? ' checked' : '' }}/>
            <label for="someSwitchOptionSuccess" class="label-success"></label>
        </div>

        @if ($errors->has('status'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>

        function getQueryVar(url,varName){
            var queryStr = unescape(url) + '&';
            var regex = new RegExp('.*?[&\\?]' + varName + '=(.*?)&.*');
            var val = queryStr.replace(regex, "$1");
            return val == queryStr ? false : val;
        }

        $(document).ready(function(){
            $('.select-field').select2();

            @if($model->youtube)
                let url = "{{$model->youtube}}";
                let v = getQueryVar(url,'v');
                let link = 'https://img.youtube.com/vi/'+v+'/default.jpg';
                $('.youtube-preview').prop('src',link);
            @endif

            $("input[name='youtube']").on('change',function () {
                let v = getQueryVar($(this).val(),'v');

                let link = 'https://img.youtube.com/vi/'+v+'/default.jpg';

                $('.youtube-preview').prop('src',link);
            });
        })
    </script>
@endpush
