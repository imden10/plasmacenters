<h4>Добавьте аудио треки</h4>

<select class="select2-field" multiple="multiple" name="audio[]" id="audio" style="width: 100%">
    <?php
        $sel = \App\Models\AudioBlogArticles::query()->where('blog_articles_id',$model->id)->pluck('audio_id')->toArray();
    ?>
    @foreach(\App\Models\Audio::query()->get() as $item)
        <option value="{{$item->id}}" @if(in_array($item->id,$sel)) selected @endif>{{$item->name}}</option>
    @endforeach
</select>
