<?php
$segment  = 1;
$allLangs = ['ru', 'uk'];
$route    = request()->segment($segment);

if (in_array($route, $allLangs)) {
    $segment = 2;
}

$route = request()->segment($segment);
$slug  = request()->segment($segment + 1);

$data = [];

switch ($route) {
    case null:
        $model = \App\Models\Landing::query()->where('slug', 'main')->first();

        if (!$model)
            break;

        $data = [
            'title'       => $model->getTranslation(app()->getLocale())->meta_title,
            'description' => $model->getTranslation(app()->getLocale())->meta_description,
            'image'       => get_image_uri($model->image)
        ];
        break;
    case 'stati-donorom-suuuper-prosto':
        $model = \App\Models\Landing::query()->where('slug', 'stati-donorom-suuuper-prosto')->first();

        if (!$model)
            break;

        $data = [
            'title'       => $model->getTranslation(app()->getLocale())->meta_title,
            'description' => $model->getTranslation(app()->getLocale())->meta_description,
            'image'       => get_image_uri($model->image)
        ];
        break;
    case 'about':
        $model = \App\Models\Landing::query()->where('slug', 'about')->first();

        if (!$model)
            break;

        $data = [
            'title'       => $model->getTranslation(app()->getLocale())->meta_title,
            'description' => $model->getTranslation(app()->getLocale())->meta_description,
            'image'       => get_image_uri($model->image)
        ];
        break;
    case 'plasmacenters':
        $data = [
            'title'       => app(Setting::class)->get('plasmacenters_title'),
            'description' => app(Setting::class)->get('plasmacenters_description'),
            'image'       => null
        ];
        break;
    case 'plasmacenter':
        $model = \App\Models\Plasmacenters::query()->where('slug', $slug)->first();

        if (!$model)
            break;

        $data = [
            'title'       => $model->getTranslation(app()->getLocale())->meta_title,
            'description' => $model->getTranslation(app()->getLocale())->meta_description,
            'image'       => get_image_uri($model->image)
        ];
        break;
    case 'donors':
        $data = [
            'title'       => app(Setting::class)->get('donors_title'),
            'description' => app(Setting::class)->get('donors_description'),
            'image'       => null
        ];
        break;
    case 'donor':
        $model = \App\Models\Donors::query()->where('slug', $slug)->first();

        if (!$model)
            break;

        $data = [
            'title'       => $model->getTranslation(app()->getLocale())->meta_title,
            'description' => $model->getTranslation(app()->getLocale())->meta_description,
            'image'       => get_image_uri($model->image)
        ];
        break;
    case 'news':
        if (!is_null($slug)) {
            $model = \App\Models\BlogArticles::query()->where('slug', $slug)->first();

            if (!$model)
                break;

            $data = [
                'title'       => $model->getTranslation(app()->getLocale())->meta_title,
                'description' => $model->getTranslation(app()->getLocale())->meta_description,
                'image'       => get_image_uri($model->image)
            ];
        } else {
            $data = [
                'title'       => app(Setting::class)->get('news_title'),
                'description' => app(Setting::class)->get('news_description'),
                'image'       => null
            ];
        }
        break;
    case 'faq':
        $data = [
            'title'       => app(Setting::class)->get('faq_title'),
            'description' => app(Setting::class)->get('faq_description'),
            'image'       => null
        ];
        break;
    default:
        $slug  = request()->segment($segment);
        $model = \App\Models\Pages::query()->where('slug', $slug)->first();

        if (!$model)
            break;

        $data = [
            'title'       => $model->getTranslation(app()->getLocale())->meta_title,
            'description' => $model->getTranslation(app()->getLocale())->meta_description,
            'image'       => get_image_uri($model->image)
        ];
}
?>

@isset($data['title'])
    <title>{{$data['title']}}</title>
@else
    <title>{{env('APP_NAME')}}</title>
@endisset

<meta name="test" content="{{env('APP_URL') . '/media/images/original/images/no-image.png'}}">

@if(count($data))
    @if((! isset($data['image'])) || (isset($data['image']) && $data['image'] === env('APP_URL') . '/media/images/original/images/no-image.png'))
        <?php $data['image'] = get_image_uri(app(Setting::class)->get('default_og_image',\App\Models\Langs::getDefaultLangCode())); ?>
    @endif

    @isset($data['description'])
        <meta name="description" content="{{$data['description']}}">
    @endisset

    @isset($data['title'])
        <meta property="og:title" content="{{$data['title']}}"/>
    @else
        <meta property="og:title" content="{{env('APP_NAME')}}"/>
    @endisset

    @isset($data['description'])
        <meta property="og:description" content="{{$data['description']}}"/>
    @endisset

    <meta property="og:url" content="{{url()->current()}}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:locale" content="{{app()->getLocale()}}"/>
    <meta property="og:site_name" content="{{env('APP_NAME')}}"/>
    @isset($data['image'])
        <meta property="og:image" content="{{$data['image']}}"/>
    @endisset
@endif

{!!  app(Setting::class)->get('head_code',\App\Models\Langs::getDefaultLangCode()) !!}
