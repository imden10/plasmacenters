<?php

use Illuminate\Support\Facades\Request;

?>

<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="pt-4">
                <li class="sidebar-item @if(in_array(Request::segment(1),['admin']) && Request::segment(2) == '') active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin')}}" aria-expanded="false">
                        <i class="mdi mdi-blur-linear"></i>
                        <span class="hide-menu">Панель приборов</span>
                    </a>
                </li>

{{--                <li class="sidebar-item @if(in_array(Request::segment(2),['users'])) active @endif">--}}
{{--                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('users.index')}}" aria-expanded="false">--}}
{{--                        <i class="mdi mdi-account"></i>--}}
{{--                        <span class="hide-menu">Пользователи</span>--}}
{{--                    </a>--}}
{{--                </li>--}}

                <li class="sidebar-item @if(in_array(Request::segment(2),['pages'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('pages.index')}}" aria-expanded="false">
                        <i class="mdi mdi-book-open-page-variant"></i>
                        <span class="hide-menu">Страницы</span>
                    </a>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['landing'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('landing.index')}}" aria-expanded="false">
                        <i class="mdi mdi-view-list"></i>
                        <span class="hide-menu">Лендинги</span>
                    </a>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['donors'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('donors.index')}}" aria-expanded="false">
                        <i class="mdi mdi-medical-bag"></i>
                        <span class="hide-menu">Доноры</span>
                    </a>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['plasmacenters'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('plasmacenters.index')}}" aria-expanded="false">
                        <i class="mdi mdi-hospital-building"></i>
                        <span class="hide-menu">Плазмацентры</span>
                    </a>
                </li>

                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-newspaper"></i>
                        <span class="hide-menu">Блог</span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item @if(in_array(Request::segment(3),['articles'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('articles.index')}}" aria-expanded="false">
                                <i class="mdi mdi-message-outline"></i>
                                <span class="hide-menu">Публикации</span>
                            </a>
                        </li>

                        <li class="sidebar-item @if(in_array(Request::segment(3),['tags'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('tags.index')}}" aria-expanded="false">
                                <i class="mdi mdi-tag"></i>
                                <span class="hide-menu">Теги</span>
                            </a>
                        </li>

                        <li class="sidebar-item @if(in_array(Request::segment(3),['subscribe'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('subscribe.index')}}" aria-expanded="false">
                                <i class="mdi mdi-email"></i>
                                <span class="hide-menu">Подписки</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['faq'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('faq.index')}}" aria-expanded="false">
                        <i class="mdi mdi-comment-question-outline"></i>
                        <span class="hide-menu">Частые вопросы</span>
                    </a>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['request'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('request.index')}}" aria-expanded="false">
                        <i class="mdi mdi-pen"></i>
                        <span class="hide-menu">Онлайн записи</span>
                    </a>
                </li>

                <li class="sidebar-item @if(in_array(Request::segment(2),['users-admin'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('users-admin.index')}}" aria-expanded="false">
                        <i class="mdi mdi-account"></i>
                        <span class="hide-menu">Администраторы</span>
                    </a>
                </li>

                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-format-paint"></i>
                        <span class="hide-menu">Внешний вид </span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">

                        <li class="sidebar-item @if(in_array(Request::segment(2),['widgets'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin/widgets" aria-expanded="false">
                                <i class="mdi mdi-widgets"></i>
                                <span class="hide-menu">Виджеты</span>
                            </a>
                        </li>

                        <li class="sidebar-item @if(in_array(Request::segment(2),['menu'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('menu.index')}}" aria-expanded="false">
                                <i class="mdi mdi-menu"></i>
                                <span class="hide-menu">Меню</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-audiobook"></i>
                        <span class="hide-menu">Аудио треки </span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">

                        <li class="sidebar-item @if(in_array(Request::segment(2),['audio-categories'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('audio-categories.index')}}" aria-expanded="false">
                                <i class="mdi mdi-tag"></i>
                                <span class="hide-menu">Категории</span>
                            </a>
                        </li>

                        <li class="sidebar-item @if(in_array(Request::segment(2),['audio'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('audio.index')}}" aria-expanded="false">
                                <i class="mdi mdi-audiobook"></i>
                                <span class="hide-menu">Треки</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-settings"></i>
                        <span class="hide-menu">Настройки</span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item @if(in_array(Request::segment(4),['main'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin/settings/main" aria-expanded="false">
                                <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                <span class="hide-menu">Главные</span>
                            </a>
                        </li>

                        <li class="sidebar-item @if(in_array(Request::segment(4),['contacts'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin/settings/contacts" aria-expanded="false">
                                <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                <span class="hide-menu">Контакты</span>
                            </a>
                        </li>
                        <li class="sidebar-item @if(in_array(Request::segment(4),['blog'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin/settings/blog" aria-expanded="false">
                                <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                <span class="hide-menu">Блог</span>
                            </a>
                        </li>
                        <li class="sidebar-item @if(in_array(Request::segment(4),['page'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin/settings/page" aria-expanded="false">
                                <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                <span class="hide-menu">Страницы</span>
                            </a>
                        </li>
                        <li class="sidebar-item @if(in_array(Request::segment(4),['donors'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin/settings/donors" aria-expanded="false">
                                <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                <span class="hide-menu">Доноры</span>
                            </a>
                        </li>
                        <li class="sidebar-item @if(in_array(Request::segment(4),['plasmacenters'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin/settings/plasmacenters" aria-expanded="false">
                                <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                <span class="hide-menu">Плазмацентры</span>
                            </a>
                        </li>
                        <li class="sidebar-item @if(in_array(Request::segment(4),['faq'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin/settings/faq" aria-expanded="false">
                                <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                <span class="hide-menu">Частые вопросы</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
