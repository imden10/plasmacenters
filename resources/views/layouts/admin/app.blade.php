<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ env('APP_NAME') }}</title>

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('matrix/images/favicon.png')}}">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <!-- Custom CSS -->
    <link href="{{asset('matrix/libs/flot/css/float-chart.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('matrix/css/style.min.css')}}" rel="stylesheet">
    <!-- summernote -->
    {{--    <link rel="stylesheet" href="{{asset("assets/plugins/summernote/summernote-bs4.css")}}">--}}
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <!-- Code Mirror Styles -->
    <link rel="stylesheet" href="{{ asset('vendor/admin/codemirror/lib/codemirror.css') }}">
    <style>
        .card {
            box-shadow: 0 0 1px rgb(0 0 0 / 13%), 0 1px 3px rgb(0 0 0 / 20%);
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 0 solid rgba(0, 0, 0, .125);
            border-radius: .25rem;
        }

        .card.card-outline {

        }

        .card.card-outline.card-info {
            border-top: 3px solid #17a2b8;
        }
    </style>
    @stack('styles')
</head>
<body>
<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
    @csrf
</form>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"
     data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">

    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
@include('layouts.admin.components.header')

<!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    @include('layouts.admin.components.aside')

    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
    @yield('breadcrumb')
    <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            @include('layouts.admin.components._errors')
            @yield('content')
        </div>

        <footer class="footer text-center">
            <strong>Copyright &copy; {{date('Y')}} <a href="{{url('/admin')}}">{{env('APP_NAME')}}</a>.</strong>
            All rights reserved.
            <b>Version</b> 1.0.0
        </footer>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="{{asset('matrix/js/sweetalert2.all.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('js/custom.js') }}"></script>
<script src="{{ asset('matrix/js/jquery-ui.min.js') }}"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Latest compiled JavaScript -->
{{--<script src="{{ asset('vendor/admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>--}}
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        crossorigin="anonymous"></script>
{{--<script src="{{asset('matrix/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>--}}
<script src="{{asset('matrix/extra-libs/sparkline/sparkline.js')}}"></script>
<!--Wave Effects -->
<script src="{{asset('matrix/js/waves.js')}}"></script>
<!--Menu sidebar -->
<script src="{{asset('matrix/js/sidebarmenu.js')}}"></script><!--Custom JavaScript -->
<script src="{{asset('matrix/js/custom.min.js')}}"></script>
<!--This page JavaScript -->
{{--<script src="matrix/js/pages/dashboards/dashboard1.js"></script>--}}
<!-- Charts js Files -->
<script src="{{asset('matrix/libs/flot/excanvas.js')}}"></script>
<script src="{{asset('matrix/libs/flot/jquery.flot.js')}}"></script>
<script src="{{asset('matrix/libs/flot/jquery.flot.pie.js')}}"></script>
<script src="{{asset('matrix/libs/flot/jquery.flot.time.js')}}"></script>
<script src="{{asset('matrix/libs/flot/jquery.flot.stack.js')}}"></script>
<script src="{{asset('matrix/libs/flot/jquery.flot.crosshair.js')}}"></script>
{{--<script src="{{asset('matrix/libs/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>--}}

{{--<script src="{{asset('matrix/js/pages/chart/chart-page-init.js')}}"></script>--}}
<!-- Summernote -->
{{--<script src="{{asset("assets/plugins/summernote/summernote-bs4.js")}}"></script>--}}
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
{{--<script src="{{asset('myLib/summernote/summernote-bs4.min.js')}}"></script>--}}

<script>
    // Define function to open filemanager window
    const lfm = function (options, cb) {
        const route_prefix = (options && options.prefix) ? options.prefix : '/filemanager';
        window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
        window.SetUrl = cb;
    };

    // Define LFM summernote button
    const LFMButton = function (context) {
        const ui = $.summernote.ui;
        const button = ui.button({
            contents: '<i class="note-icon-picture"></i> ',
            tooltip: 'Вставить изображение с файлового менеджера',
            click: function () {
                lfm({type: 'image', prefix: '/filemanager'}, function (lfmItems, path) {
                    lfmItems.forEach(function (lfmItem) {
                        context.invoke('insertImage', lfmItem.url);
                    });
                });

            }
        });

        return button.render();
    };

    const summernote_options = {
        height: 250,
        minHeight: null,
        maxHeight: null,
        toolbar: [
            ['style', ['style']], ['font', ['bold', 'underline', 'clear']], ['fontname', ['fontname']], ['color', ['color']], ['para', ['ul', 'ol', 'paragraph']], ['table', ['table']], ['insert', ['link', 'video']], ['view', ['fullscreen', 'codeview', 'help']],
            ['popovers', ['lfm']],
        ],
        buttons: {
            lfm: LFMButton
        }
    };

    $('.editor').summernote(summernote_options);
</script>
<!-- Code Mirror Scripts -->
<script src="{{ asset('vendor/admin/codemirror/lib/codemirror.js') }}"></script>
<script src="{{ asset('vendor/admin/codemirror/addon/selection/selection-pointer.js') }}"></script>
<script src="{{ asset('vendor/admin/codemirror/mode/xml/xml.js') }}"></script>
<script src="{{ asset('vendor/admin/codemirror/mode/javascript/javascript.js') }}"></script>
<script src="{{ asset('vendor/admin/codemirror/mode/css/css.js') }}"></script>
<script src="{{ asset('vendor/admin/codemirror/mode/vbscript/vbscript.js') }}"></script>
<script src="{{ asset('vendor/admin/codemirror/mode/htmlmixed/htmlmixed.js') }}"></script>
<script type="text/javascript">
    const mixedMode = {
        name: "htmlmixed",
        scriptTypes: [{
            matches: /\/x-handlebars-template|\/x-mustache/i,
            mode: null
        },
            {
                matches: /(text|application)\/(x-)?vb(a|script)/i,
                mode: "vbscript"
            }]
    };
</script>
<!-- Open media window start -->
<script src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
<script type="text/javascript">
    (function () {
        const imagePlaceholder = '{{ asset('/images/no-image.png') }}';
        const videoPlaceholder = '{{ asset('vendor/admin/images/video-placeholder.png') }}';

        $(document).on('click', '.choice-media, .choice-file', function () {
            const trigger = this;
            const media_selection = $(trigger).hasClass('choice-media');
            const file_selection = $(trigger).hasClass('choice-file');
            const media_prefix = '{{ config('filesystems.disks.public.url') }}/' + (file_selection ? 'files' : 'media');

            let type = 'image';

            if (file_selection) type = 'file';

            window.open('/filemanager?type=' + type, 'FileManager', 'width=900,height=600');

            window.SetUrl = function (items) {
                const file_path = items.map(function (item) {
                    item.url_absolute = item.url;
                    item.url = item.url.replace(media_prefix, '');

                    return item;
                });

                if (file_path.length > 0) {
                    const fullFileUrl = file_path[0].url_absolute;
                    const fileUrl = file_path[0].url;

                    console.log(file_path);

                    if (media_selection) {
                        const mediaWrapper = $(trigger).parents('.media-wrapper');

                        if (file_path[0].is_image) {
                            if (fullFileUrl.split('.').pop().toLowerCase() == 'svg') {
                                $(mediaWrapper).find('.image-tag').attr('src', '/storage/media' + fileUrl);
                            } else {
                                $(mediaWrapper).find('.image-tag').attr('src', fullFileUrl);
                            }

                            $(mediaWrapper).find('.media-input').val(fileUrl);
                        }

                        if (file_path[0].is_file) {
                            if (['mp4', 'm4v', '3gp'].indexOf(fullFileUrl.split('.').pop().toLowerCase()) !== -1) {
                                $(imageWrapper).find('.image-tag').attr('src', videoPlaceholder);
                            } else {
                                $(trigger).parent().parent().find('.file-path-field').val(fileUrl);
                            }

                            $(mediaWrapper).find('.media-input').val(fileUrl);
                        }
                    }

                    if (file_selection) {
                        // const inputFilePath = $(trigger).parents('.form-group').find('.file-path-field');
                        const inputFilePath = $(trigger).parents('.media-wrapper').find('.file-path-field');

                        if (inputFilePath) {
                            inputFilePath.val(fileUrl);
                        }
                    }
                }
            };
        });

        $(document).on('click', '.remove-media', function () {
            const mediaWrapper = $(this).parents('.media-wrapper');

            $(mediaWrapper).find('.image-tag').attr('src', imagePlaceholder);
            $(mediaWrapper).find('.media-input').val('');
        });
    })();
</script>
@stack('scripts')
</body>
</html>
