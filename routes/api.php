<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::POST('settings/all', [\App\Http\Controllers\Api\SettingsController::class,'getAll']);
Route::POST('get-main-page', [\App\Http\Controllers\Api\MainController::class,'getMain']);
Route::POST('page/get-by-slug', [\App\Http\Controllers\Api\PagesController::class,'getBySlug']);
Route::POST('landing/get-by-slug', [\App\Http\Controllers\Api\LandingController::class,'getBySlug']);
Route::POST('donors/get-by-slug', [\App\Http\Controllers\Api\DonorsController::class,'getBySlug']);
Route::POST('donors/all', [\App\Http\Controllers\Api\DonorsController::class,'getAll']);
Route::POST('plasmacenters/all', [\App\Http\Controllers\Api\PlasmacentersController::class,'getAll']);
Route::POST('plasmacenters/get-by-slug', [\App\Http\Controllers\Api\PlasmacentersController::class,'getBySlug']);
Route::POST('menu/get-by-name', [\App\Http\Controllers\Api\MenuController::class,'getByName']);
Route::POST('menu/get-by-id', [\App\Http\Controllers\Api\MenuController::class,'getById']);
Route::POST('menu/get-by-ids', [\App\Http\Controllers\Api\MenuController::class,'getByIds']);
Route::POST('request/add', [\App\Http\Controllers\Api\RequestController::class,'addRequest']);
Route::POST('request/feedback', [\App\Http\Controllers\Api\RequestController::class,'feedback']);
Route::POST('request/callback', [\App\Http\Controllers\Api\RequestController::class,'callback']);
Route::POST('news/get-by-slug', [\App\Http\Controllers\Api\BlogArticlesController::class,'getBySlug']);
Route::POST('news/all', [\App\Http\Controllers\Api\BlogArticlesController::class,'getAll']);
Route::POST('news/subscribe', [\App\Http\Controllers\Api\SubscribeController::class,'subscribe']);
