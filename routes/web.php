<?php

use App\Http\Middleware\LocaleMiddleware;
use Illuminate\Support\Facades\Route;

/* @var $localeMiddleware LocaleMiddleware */
$localeMiddleware = app( LocaleMiddleware::class);

\Illuminate\Support\Facades\Auth::routes();

Route::get('cache-clear',function (){
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');

    return redirect()->back()->with('success','Кеш успешно очищено!');
})->name('cache-clear');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/admin/login', [\App\Http\Controllers\Admin\HomeController::class,'login'])->name('admin.login');

/*********************************************** ADMIN ****************************************************************/
Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function () {
    Route::post('/export-table', [\App\Http\Controllers\Admin\ExportTableController::class,'export'])->name('export-table');
    Route::get('/', [\App\Http\Controllers\Admin\HomeController::class,'index'])->name('admin');
    Route::resource('/users-admin', \App\Http\Controllers\Admin\UserAdminController::class);
    Route::get('/users/{id}/{tab}', [\App\Http\Controllers\Admin\UsersController::class,'getTab'])->where('tab', '[a-z0-9-]+');
    Route::post('/users-change-status', [\App\Http\Controllers\Admin\UsersController::class,'changeStatus'])->name('user.change-status');
    Route::resource('/users', \App\Http\Controllers\Admin\UsersController::class);
    Route::post('/pages/menu/add-item', [\App\Http\Controllers\Admin\PagesController::class,'addItem'])->name('page.add-item');
    Route::resource('/pages', \App\Http\Controllers\Admin\PagesController::class);
    Route::resource('/landing', \App\Http\Controllers\Admin\LandingController::class);
    Route::resource('/donors', \App\Http\Controllers\Admin\DonorsController::class);
    Route::post('/plasmacenters/add-social', [\App\Http\Controllers\Admin\PlasmacenterController::class,'addSocial'])->name('plasmacenters.add-social');
    Route::resource('/plasmacenters', \App\Http\Controllers\Admin\PlasmacenterController::class);
    Route::post('menu/move', [\App\Http\Controllers\Admin\MenuController::class,'move']);
    Route::post('menu/rebuild', [\App\Http\Controllers\Admin\MenuController::class,'rebuild']);
    Route::post('menu/add-item', [\App\Http\Controllers\Admin\MenuController::class,'addItem'])->name('menu.add-item');
    Route::post('menu/delete-menu', [\App\Http\Controllers\Admin\MenuController::class,'deleteMenu'])->name('menu.delete-menu');
    Route::post('menu/add-menu', [\App\Http\Controllers\Admin\MenuController::class,'addMenu'])->name('menu.add-menu');
    Route::resource('/menu', \App\Http\Controllers\Admin\MenuController::class);
    Route::resource('/faq', \App\Http\Controllers\Admin\FaqController::class);
    Route::get('settings/{tab}', [\App\Http\Controllers\Admin\SettingsController::class,'index'])->name('add-notice')->where('tab', '[a-z0-9-]+');
    Route::post('settings/save', [\App\Http\Controllers\Admin\SettingsController::class,'save'])->name('settings.save');
    Route::resource('/audio', \App\Http\Controllers\Admin\AudioController::class);
    Route::resource('/audio-categories', \App\Http\Controllers\Admin\AudioCategoryController::class);
    Route::post('/request/status/change', [\App\Http\Controllers\Admin\RequestController::class,'changeStatus'])->name('request.status-change');
    Route::resource('/request', \App\Http\Controllers\Admin\RequestController::class);

    /** BLOG **/
    Route::group(['prefix' => 'blog'], function () {
        Route::resource('/tags', \App\Http\Controllers\Admin\BlogTagsController::class);
        Route::resource('/articles', \App\Http\Controllers\Admin\BlogArticlesController::class);
        Route::get('/subscribe', [\App\Http\Controllers\Admin\BlogSubscribeController::class,'index'])->name('subscribe.index');
        Route::get('/subscribe/list', [\App\Http\Controllers\Admin\BlogSubscribeController::class,'list'])->name('subscribe.list');
    });
});
/************************************ END ADMIN ***********************************************************************/

Route::group(['namespace' => 'Front'], function(){
    Route::any('/{any}', [\App\Http\Controllers\Front\HomeController::class,'index'])->where('any', '^(?!api).*$')->name('home');


    Route::any('/donors/{donor}', [\App\Http\Controllers\Front\HomeController::class,'index'])->where('any', '^(?!api).*$')->name('home');
    Route::any('/{any}', [\App\Http\Controllers\Front\HomeController::class,'index'])->where('any', '^(?!api).*$')->name('home');
});
/*******************************************************************/
